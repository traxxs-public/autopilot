const Job = require('../../lib/jobs/Job'),
    utils = require('../../lib/utils');

class RemoveCodeCheckpoints extends Job {

    name = 'remove_code_checkpoints';

    description = 'Remove code checkpoints';

    inputs = {
        code: {
            type: 'string',
            description: 'Code snippet'
        }
    };

    outputs = {
        code: {
            type: 'string',
            description: 'Code snippet without checkpoints'
        }
    };

    execute = async (context) => {
        let code = context.code;
        return {code: utils.removeMatchingLines(code, "$checkPoint$(")};
    }

}

module.exports = RemoveCodeCheckpoints;