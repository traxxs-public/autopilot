const checkpoint_string = "$CheckPoint$";

const self = {

    $checkPoint$: (key, data) => {
        console.log(checkpoint_string + " " + key + ": ```", JSON.stringify(data) + "```");
    },

    parseCheckPoints: (test_output) => {
        let checkpoints = {},
            index;
        while ((index = test_output.indexOf(checkpoint_string)) !== -1) {
            let key_start_index = index + checkpoint_string.length + 1,
                key_end_index = test_output.indexOf(":", key_start_index),
                key = test_output.substring(key_start_index, key_end_index).trim();
                data_start_index = test_output.indexOf('```', key_end_index) + 3,
                data_end_index = test_output.indexOf('```', data_start_index),
                data = test_output.substring(data_start_index, data_end_index);
            checkpoints[key] = checkpoints[key] || [];
            try{
                checkpoints[key].push(JSON.parse(data));
            }catch(err){
                checkpoints[key].push(undefined);
            }
            test_output = test_output.substring(data_end_index);
        }
        return checkpoints;
    }

}

module.exports = self;