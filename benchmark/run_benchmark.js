const Job = require('../lib/jobs/Job'),
    { pipe } = require('../lib/jobs/jobs'),
    utils = require('../lib/utils'),
    _ = require('lodash');

class RunBenchmark extends Job {

    name = 'run_benchmark';

    description = 'Run a benchmark';

    inputs = {
        dataset: {
            type: 'array',
            description: 'Input/Output list'
        },
        pipeline: {
            type: 'array',
            description: 'Pipeline to run'
        },
        run_count: {
            type: 'integer',
            description: 'Number of times to run the benchmark',
            default_value: 1
        },
        async: {
            type: 'boolean',
            description: 'Whether to run the benchmark asynchronously or not',
            default_value: false
        },
        apply_context: {
            type: 'object',
            description: 'Context to apply to the pipeline',
            optional: true
        }
    };

    outputs = {
        score: {
            type: 'number',
            description: 'Benchmark score'
        },
        results: {
            type: 'array',
            description: 'Detailed Results'
        }
    };

    execute = async (context) => {
        let results = [];
        console.time('Total Benchmark Time');
        for(let i = 0; i < context.run_count; i++){
            console.log(`Benchmark Run ${i + 1}\n====================`);
            console.time(`Benchmark Run ${i + 1}`);
            let run_results = await run(context.dataset, context.pipeline, context.async, context.apply_context);
            console.timeEnd(`Benchmark Run ${i + 1}`);
            results = results.concat(run_results);
        }
        let total_score = 0;
        for(let result of results){
            total_score += result.score;
        }
        let score = _.round(100 * total_score / results.length, 2);
        console.timeEnd('Total Benchmark Time');
        return {score, results};
    }

}

async function run(dataset, pipeline, async, apply_context){
    let results = [];
    if(async){
        const promises = _.map(dataset, (data, index) => {return runData(index, data, pipeline, results, apply_context)});
        await utils.awaitAll(promises);
    }else{
        for(let i = 0; i < dataset.length; i++){
            let data = dataset[i];
            await runData(i, data, pipeline, results, apply_context);
        }
    }
    return results;
}

async function runData(index, data, pipeline, results, apply_context = {}){
    console.log(`Running ${index + 1}`);
    let result = await pipe(pipeline)(_.assign(_.cloneDeep(data.input), apply_context));
    let score = await data.evaluate(data.input, result, data.evaluation, apply_context);
    console.log(`Score ${index + 1}: ${score}`);
    results.push({result, score});
}

module.exports = RunBenchmark;