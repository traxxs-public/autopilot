module.exports = [

    {
        input: {
            project_path: "side-flip",
            file_path: "utils/paths.js",
            function_name: "getParentPath",
            test_case: "Should return the parent path of a given path"
        },
        evaluation: {
            checkpoints: {
                '0': {$exists: true, $each: {path: {$test: (value) => value.split('/').length > 1}}}
            }
        }
    },
    {
        input: {
            project_path: "side-flip",
            file_path: "utils/paths.js",
            function_name: "getParentPathList",
            test_case: "Should return the list of parent paths of a given path"
        },
        evaluation: {
            checkpoints: {
                '0': {$exists: true, $each: {path: {$test: (value) => value.split('/').length > 1}}}
            }
        }
    },
    {
        input: {
            project_path: "side-flip",
            file_path: "utils/paths.js",
            function_name: "getTargetsCommonParent",
            test_case: "Should return an empty string when the target list is empty"
        },
        evaluation: {
            checkpoints: {
                '0': {$exists: true},
                '1': {$exists: false}
            }
        }
    },
    {
        input: {
            project_path: "side-flip",
            file_path: "utils/paths.js",
            function_name: "getTargetsCommonParent",
            test_case: "Should return the common parent of the given targets"
        },
        evaluation: {
            checkpoints: {
                '0': {$exists: true, $each: {target_list: {$size: {min: 1}}}},
                '1': {$exists: true},
                '2': {$exists: true, $each: {common_parent: {$ne: ""}}}
            }
        }
    },
    {
        input: {
            project_path: "side-flip",
            file_path: "utils/async.js",
            function_name: "asyncMap",
            test_case: "Should handle empty list"
        },
        evaluation: {
            checkpoints: {
                '0': {$size: {min: 1}, $each: {list: {$size: 0}}},
                '1': {$exists: false}
            }
        }
    },
    {
        input: {
            project_path: "side-flip",
            file_path: "utils/async.js",
            function_name: "asyncMap",
            test_case: "Should process single item list correctly"
        },
        evaluation: {
            checkpoints: {
                '0': {$exists: true, $each: {list: {$size: 1}}},
                '1': {$exists: true},
                '3': {$exists: true},
                '3_1': {$exists: true, $size: 1},
                '3_2': {$exists: true}
            }
        }
    },
    {
        input: {
            project_path: "side-flip",
            file_path: "utils/async.js",
            function_name: "asyncMap",
            test_case: "Should keep order when keep_order option is true"
        },
        evaluation: {
            checkpoints: {
                '0': {$exists: true, $each: {list: {$size: {min: 2}}}},
                '1': {$exists: true},
                '2': {$exists: true},
                '2_2': {$exists: true}
            }
        }
    },
    {
        input: {
            project_path: "side-flip",
            file_path: "utils/objects.js",
            function_name: "checkQueryOperatorMatch",
            test_case: "should return true for $exists operator with true and defined value"
        },
        evaluation: {
            checkpoints: {
                '0': {$exists: true, $each: { value: {$exists: true}, operator_key: '$exists', operator_value: true }}
            }
        }
    },
    {
        input: {
            project_path: "side-flip",
            file_path: "utils/objects.js",
            function_name: "checkQueryOperatorMatch",
            test_case: "should return true for $in operator with value in array"
        },
        evaluation: {
            checkpoints: {
                '0': {$exists: true, $each: { value: {$exists: true}, operator_key: '$in', operator_value: {$size: {min: 2}} }},
                '1_1': {$exists: true}
            }
        }
    },
    {
        input: {
            project_path: "side-flip",
            file_path: "utils/objects.js",
            function_name: "checkQueryOperatorMatch",
            test_case: "should return true for $regex operator with matching pattern"
        },
        evaluation: {
            checkpoints: {
                '0': {$exists: true, $each: { value: {$exists: true}, operator_key: '$regex', operator_value: {$exists: true} }},
                '2': {$exists: true, $each: { match: true }}
            }
        }
    },
    {
        input: {
            project_path: "side-flip",
            file_path: "utils/objects.js",
            function_name: "checkQueryOperatorMatch",
            test_case: "should return false for $regex operator with non-matching pattern"
        },
        evaluation: {
            checkpoints: {
                '0': {$exists: true, $each: { value: {$exists: true}, operator_key: '$regex', operator_value: {$exists: true} }},
                '2': {$exists: true, $each: { match: false }}
            }
        }
    },
    {
        input: {
            project_path: "side-flip",
            file_path: "utils/objects.js",
            function_name: "checkQueryOperatorMatch",
            test_case: "should return true for $size operator with matching length range"
        },
        evaluation: {
            checkpoints: {
                '0': {$exists: true, $each: { value: {$size: {min: 1}}, operator_key: '$size', operator_value: {min: {$exists: true}, max: {$exists: true}} }},
                '3': {$exists: true, $each: { match: true }}
            }
        }
    },
    {
        input: {
            project_path: "side-flip",
            file_path: "utils/objects.js",
            function_name: "checkQueryOperatorMatch",
            test_case: "should return false for a non-existing operator"
        },
        evaluation: {
            checkpoints: {
                '0': {$exists: true, $each: { value: {$exists: true}, operator_key: {$exists: true}, operator_value: {$exists: true}}},
                '10': {$exists: true}
            }
        }
    }

];