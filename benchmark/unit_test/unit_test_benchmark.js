const { test_framework } = require('../../config.js');
const loader = require('./loader.js'),
    { jobs } = require('../../autopilot'),
    token_utils = require('../../lib/ai/token_utils'),
    fse = require('fs-extra'),
    path = require('path');

let result_folder_path = path.join(__dirname, 'results');

const basic_gpt_3_5_pipeline = require('./pipelines/basic_gpt_3-5.js'),
    basic_gpt_4_pipeline = require('./pipelines/basic_gpt_4.js'),
    basic_gpt_4o_pipeline = require('./pipelines/basic_gpt_4o.js'),
    basic_deepseek_coder_pipeline = require('./pipelines/basic_deepseek_coder.js'),
    basic_groq_llama3_8b_pipeline = require('./pipelines/basic_groq_llama3-8b.js'),
    basic_groq_llama3_70b_pipeline = require('./pipelines/basic_groq_llama3-70b.js');

async function run() {

    let dataset = loader.loadDataSet();

    // dataset = [dataset[0]];

    // let pipeline = basic_gpt_3_5_pipeline;
    // let pipeline = basic_gpt_4_pipeline;
    // let pipeline = basic_gpt_4o_pipeline;
    // let pipeline = basic_deepseek_coder_pipeline;
    // let pipeline = basic_groq_llama3_8b_pipeline;
    let pipeline = basic_groq_llama3_70b_pipeline;

    let result = await jobs.run_benchmark({dataset, pipeline, run_count: 3, async: false});

    console.log("Score: " + result.score + "%");

    token_utils.printSessionTokenUsage();

    process.exit();

}

run();