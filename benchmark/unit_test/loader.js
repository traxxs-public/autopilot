const Project = require('../../parsing/project'),
    evaluator = require('./evaluator'),
    path = require('path'),
    _ = require('lodash');

const DATASET = require('./data/dataset');

const self = {

    getProjectAbsolutePath: (project_path) => path.join(__dirname, 'projects', project_path),

    loadDataSet: () => {
        let dataset = [];
        for(let data of DATASET){
            dataset.push(self.loadData(_.cloneDeep(data)));
        }
        return dataset;
    },

    loadData: (data) => {
        let input_data = data.input;
        let project = new Project(input_data.project_path, self.getProjectAbsolutePath(input_data.project_path));

        project.load(input_data.file_path);
        let file = project.getFile(input_data.file_path);

        input_data.project = project;
        input_data.file = file;

        data.evaluate = evaluator.evaluate;

        return data;
    }

}

module.exports = self;