const { jobs } = require('../../../autopilot');

module.exports = [
    jobs.extract_module_function.set({
        include_dependencies: true,
        max_depth: 0,
        only_prototypes: true
    }),
    jobs.remove_code_checkpoints,
    jobs.get_unit_test_template,
    jobs.write_unit_test_code.set({ generate_count: 3, chat_options: {provider: 'openai', model: 'gpt-3.5-turbo'}  }),
    jobs.select_best_test_code
]