const { jobs } = require('../../../autopilot'),
    path = require('path'),
    fse = require('fs-extra');

const system_prompt_1 = 'As a world-class software engineer which write secure and perfect JS code, your task is to implement the unit test function corresponding to the given test case description.';
const system_prompt_2 = 'You are a world-class software engineer which write secure and proper JS code.';

const cot_prompt = fse.readFileSync(path.join(__dirname, '../job_variants/write_unit_test_code/cot_prompt_1.txt'), 'utf8');

module.exports = [
    jobs.extract_module_function.set({
        include_dependencies: true,
        max_depth: 0,
        only_prototypes: true
    }),
    jobs.remove_code_checkpoints,
    jobs.get_unit_test_template,
    jobs.write_unit_test_code.set({ chat_options: {provider: 'groq', model: 'llama3-8b-8192'}  })
    // jobs.write_unit_test_code.set({ chat_options: {provider: 'groq', model: 'llama3-8b-8192', prompt: cot_prompt}  })
]