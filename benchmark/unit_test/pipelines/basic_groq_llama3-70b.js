const { jobs } = require('../../../autopilot');

module.exports = [
    jobs.extract_module_function.set({
        include_dependencies: true,
        max_depth: 0,
        only_prototypes: true
    }),
    jobs.remove_code_checkpoints,
    jobs.get_unit_test_template,
    jobs.write_unit_test_code.set({ chat_options: {provider: 'groq', model: 'llama3-70b-8192'}  })
]