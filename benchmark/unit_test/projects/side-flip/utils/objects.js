const _ = require('lodash'),
    { $checkPoint$ } = require('../../../../utils/checkpoints');
const { match } = require('minimatch');

const self = {

    /**
     * Recursively removes properties with undefined values from an object.
     * If a property is an object itself, it will also be cleaned of undefined values.
     * This function mutates the original object.
     *
     * @param {Object} object - The object to clean of undefined properties.
     * @returns {Object} The original object with undefined values removed.
     */
    clearObjectUndefinedValues: (object) => {
        for (let prop in object) {
            let value = object[prop];
            if (value === undefined) {
                delete object[prop];
            } else if (value !== null && value.constructor === Object) {
                self.clearObjectUndefinedValues(value);
            }
        }
        return object;
    },

    /**
     * Checks if a given property is a sub-target property.
     * @param {string} property - The property to be checked.
     * @returns {boolean} - Returns true if the property contains a dot (.), indicating it is a sub-target property. Otherwise, returns false.
     */
    isSubTargetProperty: (property) => {
        return property.indexOf(".") > -1;
    },

    /**
     * This function checks if the given object matches the given attributes.
     * @param {Object} object - The object to be checked.
     * @param {Object} attributes - The attributes to be checked against.
     * @param {Boolean} apply_get - Optional. Indicates whether to apply the fillStringFromObject function to the attributes. Default is true.
     * @returns {Boolean} Returns true if the object matches the attributes, false otherwise.
     */
    objectMatchAttributes: (object, attributes, apply_get = true) => {
        let target = object, check = attributes;
        if (apply_get) {
            check = {};
            target = {};
            for (let prop in attributes) {
                let filter_target = self.fillStringFromObject(prop, object);
                _.set(check, filter_target, attributes[prop]);
            }
            for (let prop in object) {
                _.set(target, prop, object[prop]);
            }
        }
        return _.find([target], check) != null;
    },

    /**
     * Checks if an object has at least one attribute that matches the given attributes.
     * @param {object} object - The object to be checked.
     * @param {object} attributes - The attributes to be matched.
     * @returns {boolean} - Returns true if at least one attribute matches, otherwise returns false.
     */
    objectMatchAtLeastOneAttribute: (object, attributes) => {
        for (let prop in attributes) {
            if (self.objectMatchAttributes(object, { [prop]: attributes[prop] })) {
                return true;
            }
        }
        return false;
    },

    /**
     * Replaces placeholders in a string with corresponding values from an object.
     * @param {string} string - The string containing placeholders.
     * @param {object} object - The object containing values to replace the placeholders.
     * @returns {string} - The string with all placeholders replaced with corresponding values.
     */
    fillStringFromObject: (string, object) => {
        let result = string;
        while (result.indexOf("{") > -1 && result.indexOf("}") > -1) {
            let target = result.substring(result.indexOf("{") + 1, result.indexOf("}"));
            result = result.replace("{" + target + "}", _.get(object, target));
        }
        return result;
    },

    /**
     * Flattens a given object and returns a new object with the flattened structure.
     * @param {object} object - The object to be flattened.
     * @param {string} prefix - Optional prefix to be added to the flattened keys.
     * @returns {object} - The flattened object.
     */
    getFlattenedObject: (object, prefix = "") => {
        let result = {};
        for (let prop in object) {
            let value = object[prop];
            if (value != null && value.constructor === Object) {
                if (Object.keys(value).length === 0) {
                    // Handling the empty object case
                    result[prefix + prop] = {};
                } else {
                    let sub_result = self.getFlattenedObject(value, prefix + prop + ".");
                    for (let sub_prop in sub_result) {
                        result[sub_prop] = sub_result[sub_prop];
                    }
                }
            } else {
                result[prefix + prop] = value;
            }
        }
        return result;
    },

    /**
     * Unflattens a given flat object including paths with array indices, and returns a new object with the nested structure.
     * @param {object} flatObject - The flat object to be unflattened.
     * @returns {object} - The nested object with arrays and objects as needed.
     */
    unflattenObject: (flatObject) => {
        const result = {};

        for (const path in flatObject) {
            const parts = path.split('.').reduce((acc, part) => {
                // Handle array indices, splitting them out into separate parts
                if (part.includes('[')) {
                    const [p, index] = part.split('[');
                    acc.push(p, '[' + index);
                } else {
                    acc.push(part);
                }
                return acc;
            }, []);

            let currentPart = result;

            for (let i = 0; i < parts.length - 1; i++) {
                let part = parts[i];
                const nextPart = parts[i + 1];

                if (nextPart[0] === '[') {
                    // Strip the brackets and convert to an integer index
                    const index = parseInt(nextPart.slice(1, -1), 10);
                    part = part || '';

                    // Ensure the current part is an array to accommodate the index
                    if (!currentPart[part] || !Array.isArray(currentPart[part])) {
                        currentPart[part] = [];
                    }

                    // Fill in any missing array elements up to the current index
                    while (currentPart[part].length <= index) {
                        currentPart[part].push({});
                    }

                    currentPart = currentPart[part];

                    // Assign the value at the current index
                    if (i + 2 < parts.length && typeof currentPart[index] !== 'object') {
                        currentPart[index] = {};
                    } else if (i + 2 === parts.length) {
                        currentPart[index] = flatObject[path];  // Assign the value directly
                        break;
                    }

                    currentPart = currentPart[index];
                    i++; // Skip the next part as it's the array index we've just processed
                } else {
                    if (!currentPart[part] || typeof currentPart[part] !== 'object') {
                        currentPart[part] = {};
                    }

                    currentPart = currentPart[part];
                }
            }

            // Set the value at the final part if not already set (for non-array cases)
            if (parts.length > 1 && !path.endsWith(']')) {
                const finalPart = parts[parts.length - 1];
                currentPart[finalPart] = flatObject[path];
            } else if (parts.length === 1) {
                // Single key case
                currentPart[path] = flatObject[path];
            }
        }

        return result;
    },

    /**
     * Returns an object containing only the properties that have been updated.
     *
     * @param {Object} entity - The entity to update.
     * @param {Object} update - The update object containing the new property values (works with nested target properties such as 'prop1.prop2': 'value').
     * @returns {Object} - An object containing the updated properties of the entity.
     */
    getUpdatedProperties: (entity, update) => {
        let updated_props = {};
        for (let target in update) {
            if (!_.isEqual(_.get(entity, target), update[target])) {
                updated_props[target] = update[target];
            }
        }
        return updated_props;
    },

    /**
     * Updates an object with the properties from the given update object.
     *
     * @param {Object} object - The object to update.
     * @param {Object} update - The update object containing the new property values (works with nested target properties such as 'prop1.prop2': 'value').
     */
    updateObject: (object, update) => {
        for (let target in update) {
            _.set(object, target, update[target]);
        }
    },

    checkQueryOperatorMatch: (value, operator_key, operator_value) => {
        $checkPoint$(0, { value, operator_key, operator_value });
        if (operator_key === '$exists') {
            return operator_value ? value !== undefined : value === undefined;
        } else if (operator_key === '$eq') {
            return _.isEqual(value, operator_value);
        } else if (operator_key === '$ne') {
            return !_.isEqual(value, operator_value);
        } else if (operator_key === '$in') {
            for (let check_value of operator_value) {
                if (_.isEqual(check_value, value)) {
                    $checkPoint$("1_1");
                    return true;
                }
            }
            $checkPoint$("1_2");
            return false;
        } else if (operator_key === '$nin') {
            for (let check_value of operator_value) {
                if (_.isEqual(check_value, value)) {
                    return false;
                }
            }
            return true;
        } else if (operator_key === '$gt') {
            return value > operator_value;
        } else if (operator_key === '$gte') {
            return value >= operator_value;
        } else if (operator_key === '$lt') {
            return value < operator_value;
        } else if (operator_key === '$lte') {
            return value <= operator_value;
        } else if (operator_key === '$regex') {
            $checkPoint$("2", { match: new RegExp(operator_value).test(value) });
            return new RegExp(operator_value).test(value);
        } else if (operator_key === '$size') {
            let length = value.length;
            if(typeof operator_value === 'object') {
                $checkPoint$("3", { match: (operator_value.min === undefined || length >= operator_value.min) && (operator_value.max === undefined || length <= operator_value.max)});
                return (operator_value.min === undefined || length >= operator_value.min) && 
                    (operator_value.max === undefined || length <= operator_value.max);
            }else{
                return length === operator_value;
            }
        } else if (operator_key === '$elemMatch') {
            return self.objectMatchQuery(value, operator_value);
        } else if (operator_key === '$each') {
            if (!Array.isArray(value)) {
                return false;
            }
            for (let item of value) {
                if (!self.objectMatchQuery(item, operator_value)) {
                    return false;
                }
            }
            return true;
        } else if (operator_key === '$or') {
            for (let query of operator_value) {
                if ((typeof value === 'object' && self.objectMatchQuery(value, query)) ||
                    (typeof value !== 'object' && self.queryMatchCustomizer(value, query))) {
                    return true;
                }
            }
            return false;
        } else if (operator_key === '$and') {
            for (let query of operator_value) {
                if (!((typeof value === 'object' && self.objectMatchQuery(value, query)) ||
                    (typeof value !== 'object' && self.queryMatchCustomizer(value, query)))) {
                    return false;
                }
            }
            return true;
        }
        $checkPoint$(10);
        return false;
    },

    queryMatchCustomizer: (object_value, query_value) => {
        if (_.isEqual(object_value, query_value)) {
            return true;
        } else if (Array.isArray(object_value) && !Array.isArray(query_value) && _.find(object_value, query_value) != null) {
            return true;
        }
        if (query_value == null) {
            return false;
        }
        if (typeof query_value === 'object') {
            let query_keys = Object.keys(query_value);
            let operator_keys = query_keys.filter(key => key.startsWith('$'));
            if (operator_keys.length > 0) {
                for (let operator_key of operator_keys) {
                    if (!self.checkQueryOperatorMatch(object_value, operator_key, query_value[operator_key])) {
                        return false;
                    }
                }
            }
            if (query_keys.length !== operator_keys.length) {
                let query_value_without_operators = _.omit(query_value, operator_keys);
                return _.isMatchWith(object_value, query_value_without_operators, self.queryMatchCustomizer);
            } else {
                return true;
            }
        }
        return false;
    },

    getFilledObjectForCheck: (obj, query) => {
        let clone = _.cloneDeep(obj),
            flat_query = self.getFlattenedObject(query);
        for (let target in flat_query) {
            let keys = target.split('.'),
                last = keys.pop();
            if (last.includes('$')) {
                let attr_target = keys.join('.');
                if (_.get(clone, attr_target) === undefined) {
                    _.set(clone, attr_target, undefined);
                }
            } else if (flat_query[target] === undefined) {
                _.set(clone, target, undefined);
            }
        }
        return clone;
    },

    objectMatchQuery: (obj, query) => {
        let target = self.getFilledObjectForCheck(obj, query);
        query = self.unflattenObject(query);
        return _.isMatchWith(target, query, self.queryMatchCustomizer);
    }

}

module.exports = self;