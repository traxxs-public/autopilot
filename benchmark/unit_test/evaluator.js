const { jobs } = require('../../autopilot'),
    { pipe } = require('../../lib/jobs/jobs'),
    { parseCheckPoints } = require('../utils/checkpoints'),
    utils = require('side-flip/utils'),
    _ = require('lodash');

const self = {

    evaluate: async (input, output, evaluation, apply_context = {}) => {
        let score = 0;
        let context = _.assign({}, input, apply_context);
        context.test_code = output.test_code;
        try {
            let result = await pipe([
                jobs.extract_module_function,
                jobs.get_unit_test_template,
                jobs.run_unit_test
            ])(context);
            let test_results = result.test_results;
            if (test_results.success) {
                score += 0.5;
            }
            let test_checkpoints = parseCheckPoints(test_results.output),
                expected_checkpoints = evaluation.checkpoints;
            let checkpoint_count = Object.keys(expected_checkpoints).length;
            for(let key in expected_checkpoints){
                if(utils.objectMatchQuery({[key]: test_checkpoints[key]}, {[key]: expected_checkpoints[key]})){
                    score += 0.5 / checkpoint_count;
                }
            }
            score = Math.min(1, score);
        } catch (err) {
            console.log(err);
        }
        return score;
    }

}

module.exports = self;