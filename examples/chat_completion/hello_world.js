const {jobs} = require('../../autopilot'),
    token_utils = require('../../lib/ai/token_utils');

async function run(){

    let context = {
        messages: [
            {
                role: 'user',
                content: 'Hello, how are you?'
            }
        ],
        chat_options: {
            provider: 'groq',
            model: 'llama3-8b-8192'
        }
    };

    let result = await jobs.get_chat_completion(context);

    console.log(result.completion);

    token_utils.printSessionTokenUsage();

    process.exit();

}

run();