const {jobs} = require('../../autopilot'),
    token_utils = require('../../lib/ai/token_utils');

async function run(){

    let context = {
        prompt: "Output a JS function that returns the nth number in the Fibonacci sequence",
        chat_options: {
            provider: 'groq',
            model: 'llama3-8b'
        }
    };

    let result = await jobs.generate_code(context);

    console.log(result.code);

    token_utils.printSessionTokenUsage();

    process.exit();

}

run();