const _ = require('lodash');

module.exports = {

    foo: 'bar',

    testFn: function(foo, bar) {
        return foo + bar;
    }

}