const { jobs } = require('../../autopilot'),
    token_utils = require('../../lib/ai/token_utils'),
    Project = require('../../parsing/project'),
    path = require('path');

let project_path = path.join(__dirname, 'project');

let target_file_path = 'my_lib.js';

let target_function = 'testFn';

let test_case = "should return the concatenation of two strings";

async function run() {

    let project = new Project("my_project", project_path, {debug: true});

    project.load(target_file_path);

    let file = project.getFile(target_file_path);

    let context = {
        project: project,
        file: file,
        function_name: target_function,
        test_case: test_case
    };

    let result = await jobs.implement_unit_test(context);

    console.log(result.test_code);

    console.log("SUCCESS: " + result.passing_test);

    token_utils.printSessionTokenUsage();

    process.exit();

}

run();