const console_menu = require('../lib/console_menu'),
    cli_config = require('./config'),
    _ = require('lodash');

const self = async (cli_context) => {
    console.clear();
    console.log('Autopilot CLI - Select File\n');
    let files = cli_context.project.getFiles();
    // console.log(files);
    let action_list = _.map(files, (file, index) => {
        return {title: file.project_path, action: file.project_path};
    });
    action_list.push({title: 'Return', action: 'return'});
    let action = await console_menu.getActionMenu(action_list);
    if(action === 'return'){
        return;
    }else{
        let file = _.find(files, {project_path: action});
        cli_context.file = file;
        cli_config.set({file: file.project_path});
    }
}

module.exports = self;