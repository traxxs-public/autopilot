const console_menu = require('../lib/console_menu'),
    cli_config = require('./config'),
    cli_utils = require('./utils'),
    select_file = require('./select_file'),
    js_doc = require('./js_doc/js_doc'),
    unit_test = require('./unit_test/unit_test'),
    file_status = require('./file_status');

const self = async (cli_context) => {
    console.clear();
    console.log('Autopilot CLI - Main Menu\n');
    cli_context.project.matchTestFrameworkRequirements(cli_config.config.test_framework, false);
    console.log('\nSelected File: ' + (cli_context.file ? cli_context.file.project_path : 'None') + '\n');
    let action_list = [
        {title: 'Select file', action: 'select_file'}
    ];
    if(cli_context.file){
        action_list.push({title: 'JSDoc', action: 'js_doc'});
        action_list.push({title: 'Unit Test', action: 'unit_test'});
        action_list.push({title: 'File Status', action: 'file_status'});
    }
    action_list.push({title: 'Edit Config', action: 'edit_config'})
    let action = await console_menu.getActionMenu(action_list);
    if(action === 'exit'){
        return;
    }else if(action === 'select_file'){
        await select_file(cli_context);
    }else if(action === 'js_doc'){
        await js_doc(cli_context);
    }else if(action === 'unit_test'){
        await unit_test(cli_context);
    }else if(action === 'file_status'){
        await file_status(cli_context);
    }else if(action === 'edit_config'){
        cli_utils.openTextEditor(cli_config.getConfigPath());
        cli_config.load();
    }
    await self(cli_context);
}

module.exports = self;