const cli_config = require('./config'),
    { execSync } = require('child_process'),
    utils = require('../lib/utils');

const self = {

    getUnitTestsOutputPath: (project, file, function_name, absolute = true) => {
        let project_output_path = utils.computeFunctionTargetPath(cli_config.config.unit_test_output_path, file, function_name, '.test.js');
        if(absolute){
            return project.root_path + '/' + project_output_path;
        }else{
            return project_output_path;
        }
    },

    openTextEditor: (path) => {
        try {
            let editor = cli_config.config.text_editor;
            execSync(editor + ' ' + path, { stdio: 'inherit' });
            console.log('Editor closed. Continuing execution...');
        } catch (error) {
            console.error('Error occurred:', error);
        }
    },

}

module.exports = self;