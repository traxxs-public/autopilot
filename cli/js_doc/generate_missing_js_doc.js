const console_menu = require('../../lib/console_menu'),
    _ = require('lodash'),
    cli_config = require('../config'),
    { jobs } = require('../../autopilot');

const self = async (cli_context) => {
    console.clear();
    console.log('Autopilot CLI - Generate Missing JSDoc\n');
    let file = cli_context.file,
        targets = getMissingJSDocTargets(file);

    console.log('Selected File: ' + file.project_path + '\n');

    if (targets.length === 0) {
        console.log('No missing JSDoc found...');
        return await console_menu.waitForKeyPress();
    }

    console.log('Missing JSDoc Targets:');
    for (let target of targets) {
        console.log(' - ' + target.type + ' ' + target.name);
    }

    let action_list = [
        { title: 'Generate and apply JSDoc for targets', action: 'generate' },
        { title: 'Return', action: 'return' }
    ];

    let action = await console_menu.getActionMenu(action_list);

    if (action === 'return') {
        return;
    }

    console.clear();
    console.log('Autopilot CLI - Generating Missing JSDoc...\n');

    for (let target of targets) {
        try {
            let context = {
                project: cli_context.project,
                file: file,
                chat_options: cli_config.config.chat_options.js_doc
            };
            let js_doc = await generateJSDoc(context, target);
            let file_target = target.type === 'function' ? file.function_map[target.name] : file.variable_map[target.name];
            if (file_target.js_doc) {
                file.replace('\n' + file_target.js_doc, "", false);
            }
            file.insertBeforeLine([target.name + ":", target.name + " :"], js_doc);
            file_target.js_doc = js_doc;
            console.log('Generated JSDoc for ' + target.type + ' ' + target.name + '...');
        } catch (err) {
            console.log('Error generating JSDoc for ' + target.type + ' ' + target.name + '...');
        }
    }

    console.log('\nAll missing JSDoc generated and applied!');
    await console_menu.waitForKeyPress();

}

function getMissingJSDocTargets(file) {
    let targets = [];
    for (let name in file.function_map) {
        let fn = file.function_map[name];
        if (!fn.js_doc) {
            targets.push({ type: 'function', name: name });
        }
    }
    for (let name in file.variable_map) {
        let variable = file.variable_map[name];
        if (!variable.js_doc) {
            targets.push({ type: 'variable', name: name });
        }
    }
    return targets;
}

async function generateJSDoc(context, target, try_count = 0) {
    try {
        let code;
        if (target.type === 'function') {
            context.function_name = target.name;
            code = (await jobs.extract_module_function.set({
                include_dependencies: true,
                max_depth: 1,
                exclude_target_function_js_doc: true
            })(context)).code;
        } else if (target.type === 'variable') {
            context.variable_name = target.name;
            code = (await jobs.extract_module_variable.set({
                include_dependencies: true,
                max_depth: 1,
                exclude_target_variable_js_doc: true
            })(context)).code;
        }

        context.code = code;

        let js_doc;
        if (target.type === 'function') {
            js_doc = (await jobs.generate_function_js_doc(context)).js_doc;
        } else if (target.type === 'variable') {
            js_doc = (await jobs.generate_variable_js_doc(context)).js_doc;
        }
        return js_doc;
    } catch (err) {
        if (try_count < 2) {
            return generateJSDoc(context, target, try_count + 1);
        } else {
            throw err;
        }
    }
}

module.exports = self;