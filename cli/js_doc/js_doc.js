const console_menu = require('../../lib/console_menu'),
    generate_target_js_doc = require('./generate_target_js_doc'),
    generate_missing_js_doc = require('./generate_missing_js_doc');

const self = async (cli_context) => {
    console.clear();
    console.log('Autopilot CLI - JSDoc\n');
    console.log('Selected File: ' + cli_context.file.project_path + '\n');
    let action_list = [
        {title: 'Generate Target JSDoc', action: 'generate_target_js_doc'},
        {title: 'Generate Missing JSDoc', action: 'generate_missing_js_doc'},
        {title: 'Return', action: 'return'}
    ];
    let action = await console_menu.getActionMenu(action_list);
    if(action === 'return'){
        return;
    }else if(action === 'generate_target_js_doc'){
        await generate_target_js_doc(cli_context);
    }else if(action === 'generate_missing_js_doc'){
        await generate_missing_js_doc(cli_context);
    }
    await self(cli_context);
}

module.exports = self;