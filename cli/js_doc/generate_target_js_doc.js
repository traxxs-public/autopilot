const console_menu = require('../../lib/console_menu'),
    _ = require('lodash'),
    utils = require('../../lib/utils'),
    cli_config = require('../config'),
    select_target = require('../select_target'),
    { jobs } = require('../../autopilot');

const self = async (cli_context) => {
    let file = cli_context.file;
    let target = await getTarget(cli_context);
    if(!target){
        return;
    }
    console.clear();
    console.log('Autopilot CLI - Generating Target JSDoc...\n');

    let context = {
        project: cli_context.project,
        file: file,
        chat_options: cli_config.config.chat_options.js_doc
    };

    let js_doc;

    try{
        js_doc = await generateJSDoc(context, target);
    }catch(err){
        console.log('Error generating JSDoc...');
        await console_menu.waitForKeyPress();
        return;
    }

    console.log("\nGenerated JSDoc:\n\n" + js_doc);

    let action_list = [
        {title: 'Apply JSDoc', action: 'apply'},
        {title: 'Copy JSDoc to Clipboard', action: 'copy'},
        {title: 'Regenerate JSDoc', action: 'regenerate'},
        {title: 'Return', action: 'return'}
    ];

    let action = await console_menu.getActionMenu(action_list);

    if(action === 'return'){
        return;
    }else if(action === 'apply'){
        let file_target = target.type === 'function' ? file.function_map[target.name] : file.variable_map[target.name];
        if (file_target.js_doc) {
            file.replace('\n' + file_target.js_doc, "", false);
        }
        file.insertBeforeLine([target.name + ":", target.name + " :"], js_doc);
    }else if(action === 'regenerate'){
        return self(_.assign({}, cli_context, {js_doc_target: target}));
    }else if(action === 'copy'){
        await utils.copyToClipboard(js_doc);
    }

}

async function getTarget(cli_context){
    if(cli_context.js_doc_target){
        return cli_context.js_doc_target;
    }else{
        return await select_target(cli_context, {
            functions: true,
            variables: true,
            getTargetColor: (type, target) => {
                return target.js_doc ? 'green' : 'red';
            }
        });
    }
}

async function generateJSDoc(context, target, try_count = 0){
    try{
        let code;
        if(target.type === 'function'){
            context.function_name = target.name;
            code = (await jobs.extract_module_function.set({
                include_dependencies: true,
                max_depth: 1,
                exclude_target_function_js_doc: true
            })(context)).code;
        }else if(target.type === 'variable'){
            context.variable_name = target.name;
            code = (await jobs.extract_module_variable.set({
                include_dependencies: true,
                max_depth: 1,
                exclude_target_variable_js_doc: true
            })(context)).code;
        }
    
        context.code = code;
    
        let js_doc;
        if(target.type === 'function'){
            js_doc = (await jobs.generate_function_js_doc(context)).js_doc;
        }else if(target.type === 'variable'){
            js_doc = (await jobs.generate_variable_js_doc(context)).js_doc;
        }
        return js_doc;
    }catch(err){
        if(try_count < 2){
            return generateJSDoc(context, target, try_count + 1);
        }else{
            throw err;
        }
    }
}

module.exports = self;