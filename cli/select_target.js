const console_menu = require('../lib/console_menu');

const self = async (cli_context, options = {}) => {
    console.clear();
    console.log('Autopilot CLI - Select Target\n');
    let action_list = [],
        file = cli_context.file;
    if(options.variables){
        for(let name in file.variable_map){
            let color = options.getTargetColor ? options.getTargetColor('variable', file.variable_map[name]) : undefined;
            action_list.push({title: name + ' (var)', action: 'variable_' + name, color: color});
        }
    }
    if(options.functions){
        for(let name in file.function_map){
            let color = options.getTargetColor ? options.getTargetColor('function', file.function_map[name]) : undefined;
            action_list.push({title: name + ' (fn)', action: 'function_' + name, color: color});
        }
    }
    action_list.push({title: 'Return', action: 'return'});
    let action = await console_menu.getActionMenu(action_list);
    if(action === 'return'){
        return;
    }else if(action.indexOf('variable_') === 0){
        let variable_name = action.replace('variable_', '');
        return {type: 'variable', name: variable_name};
    }else if(action.indexOf('function_') === 0){
        let function_name = action.replace('function_', '');
        return {type: 'function', name: function_name};
    }
}

module.exports = self;