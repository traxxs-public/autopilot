const console_menu = require('../lib/console_menu'),
    cli_config = require('./config'),
    cli_utils = require('./utils'),
    fse = require('fs-extra'),
    chalk = require('chalk'),
    _ = require('lodash');

const self = async (cli_context) => {
    console.clear();
    console.log('Autopilot CLI - File Status\n');
    console.log('Selected File: ' + cli_context.file.project_path + '\n');
    let file = cli_context.file;
    for(let variable_name in file.variable_map){
        let js_doc_exists = file.variable_map[variable_name].js_doc !== undefined;
        console.log('- ' + variable_name + ': ' + chalk[js_doc_exists ? 'green' : 'red']('js_doc'));
    }
    for(let function_name in file.function_map){
        let js_doc_exists = file.function_map[function_name].js_doc !== undefined,
            unit_tests_exists = fse.existsSync(cli_utils.getUnitTestsOutputPath(cli_context.project, file, function_name));
        console.log('- ' + function_name + ': ' + chalk[js_doc_exists ? 'green' : 'red']('js_doc') + ' | ' + chalk[unit_tests_exists ? 'green' : 'red']('unit_tests'));
    }
    await console_menu.waitForKeyPress();
}

module.exports = self;