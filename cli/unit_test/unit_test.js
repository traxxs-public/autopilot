const console_menu = require('../../lib/console_menu'),
    generate_single_unit_test = require('./generate_single_unit_test'),
    generate_unit_test_suite = require('./generate_unit_test_suite');

const self = async (cli_context) => {
    console.clear();
    console.log('Autopilot CLI - Unit Test\n');
    console.log('Selected File: ' + cli_context.file.project_path + '\n');
    let action_list = [
        {title: 'Generate single unit test', action: 'generate_single_unit_test'},
        {title: 'Generate complete unit test suite', action: 'generate_unit_test_suite'},
        {title: 'Return', action: 'return'}
    ];
    let action = await console_menu.getActionMenu(action_list);
    if(action === 'return'){
        return;
    }else if(action === 'generate_single_unit_test'){
        await generate_single_unit_test(cli_context);
    }else if(action === 'generate_unit_test_suite'){
        await generate_unit_test_suite(cli_context);
    }
    await self(cli_context);
}

module.exports = self;