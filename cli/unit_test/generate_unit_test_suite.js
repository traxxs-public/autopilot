const console_menu = require('../../lib/console_menu'),
    _ = require('lodash'),
    chalk = require('chalk'),
    fse = require('fs-extra'),
    utils = require('../../lib/utils'),
    cli_utils = require('../utils'),
    cli_config = require('../config'),
    select_target = require('../select_target'),
    { jobs, pipe } = require('../../autopilot');

const self = async (cli_context) => {

    let target = await getTarget(cli_context);
    if (!target) {
        return;
    }
    let unit_test_cases = await getTestCases(_.assign({}, cli_context, { unit_test_target: target }));
    if (!unit_test_cases) {
        return;
    }

    console.clear();

    console.log('Autopilot CLI - Generating Unit Tests...\n');

    let context = {
        project: cli_context.project,
        file: cli_context.file,
        function_name: target.name,
        unit_test_cases,
        existing_test_code_map: cli_context.existing_test_code_map || {},
        chat_options: cli_config.config.chat_options.write_unit_test,
        skip_failed_tests: true
    };

    let result;

    try {
        result = await jobs.implement_unit_test_suite(context);
    } catch (err) {
        console.log('Error generating Unit Test...');
        console.log(err);
        await console_menu.waitForKeyPress();
        return;
    }

    let test_code = result.test_code,
        test_code_list = result.test_code_list;

    console.log('Generated Unit Tests:\n');
    console.log(test_code);

    let failed_count = 0,
        passed_test_map = {};
    console.log('\n\nTest List Status:');
    for(let i = 0; i < unit_test_cases.length; i++){
        let passing = result.passing_test_list[i],
            status = passing ? 'Pass' : 'Fail',
            color = passing ? 'green' : 'red';
        if(!passing){
            failed_count++;
        }else{
            passed_test_map[i] = test_code_list[i];
        }
        console.log((i + 1) + '. ' + unit_test_cases[i] + ": " + chalk[color](status));
    }

    let action_list = [
        { title: 'Save Unit Tests', action: 'apply'},
        { title: 'Copy Unit Tests to Clipboard', action: 'copy' }
    ];

    if(failed_count > 0){
        action_list.push({ title: 'Regenerate Failing Unit Tests (' + failed_count + ")", action: 'regenerate_failing' });
    }

    action_list = action_list.concat([
        { title: 'Regenerate All Unit Tests', action: 'regenerate' },
        { title: 'Return', action: 'return' }
    ])

    let action = await console_menu.getActionMenu(action_list);

    if (action === 'return') {
        return;
    } else if (action === 'apply') {
        await applyUnitTests(cli_context.project, cli_context.file, target.name, test_code);
    } else if (action === 'regenerate') {
        return self(_.assign({}, cli_context, { unit_test_target: target, unit_test_cases }));
    } else if (action === 'regenerate_failing') {
        return self(_.assign({}, cli_context, { unit_test_target: target, unit_test_cases, existing_test_code_map: passed_test_map }));
    } else if (action === 'copy') {
        await utils.copyToClipboard(test_code);
    }

}

async function getTarget(cli_context) {
    if (cli_context.unit_test_target) {
        return cli_context.unit_test_target;
    } else {
        return await select_target(cli_context, {
            functions: true,
            getTargetColor: (type, target) => {
                let output_path = cli_utils.getUnitTestsOutputPath(cli_context.project, cli_context.file, target.name);
                return fse.existsSync(output_path) ? 'green' : 'red';
            }
        });
    }
}

async function getTestCases(cli_context) {
    if (cli_context.unit_test_cases) {
        return cli_context.unit_test_cases;
    }
    console.clear();
    let target = cli_context.unit_test_target;
    console.log('Selected Target: ' + target.name + '\n');
    console.log('Generating Unit Test cases...');
    let context = {
        project: cli_context.project,
        file: cli_context.file,
        function_name: target.name,
        chat_options: cli_config.config.chat_options.generate_unit_test_cases
    };
    let result = await pipe([
        jobs.extract_module_function.set({
            include_dependencies: true,
            max_depth: 0,
            only_prototypes: true
        }),
        jobs.generate_unit_test_cases
    ])(context);
    let test_cases = result.unit_test_cases;
    console.log('\nGenerated test cases:\n');
    for (let i = 0; i < test_cases.length; i++) {
        console.log((i + 1) + '. ' + test_cases[i]);
    }

    let action_list = [
        { title: 'Validate', action: 'validate' },
        { title: 'Regenerate Test Cases', action: 'regenerate' },
        { title: 'Return', action: 'return' }
    ];

    let action = await console_menu.getActionMenu(action_list);

    if (action === 'return') {
        return;
    } else if (action === 'regenerate') {
        return self(cli_context);
    } else if (action === 'validate') {
        return test_cases;
    }
}

async function applyUnitTests(project, file, function_name, test_code) {
    let unit_test_output_path = cli_utils.getUnitTestsOutputPath(project, file, function_name, false);
    console.clear();
    let path = await console_menu.waitForInput('Unit test file path:', unit_test_output_path);
    path = project.root_path + '/' + path;
    if(fse.existsSync(path)){
        console.log('\nFile already exists: ' + path);
        let action = await console_menu.getActionMenu([
            { title: 'Overwrite', action: 'overwrite' },
            { title: 'Copy Unit Tests to Clipboard', action: 'copy' },
            { title: 'Cancel', action: 'cancel' }
        ]);
        if(action === 'cancel'){
            return;
        }else if (action === 'copy') {
            return await utils.copyToClipboard(test_code);
        }
    }
    fse.outputFileSync(path, test_code);
}

module.exports = self;