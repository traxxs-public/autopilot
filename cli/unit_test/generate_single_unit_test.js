const console_menu = require('../../lib/console_menu'),
    _ = require('lodash'),
    utils = require('../../lib/utils'),
    code_utils = require('../../lib/code_utils'),
    cli_config = require('../config'),
    select_target = require('../select_target'),
    { jobs } = require('../../autopilot');

const self = async (cli_context) => {

    let file = cli_context.file;
    let target = await getTarget(cli_context);
    if (!target) {
        return;
    }
    console.clear();
    console.log('Selected Target: ' + target.name + '\n');

    let test_case = await console_menu.waitForInput('Test Case Description: ');

    console.log('Autopilot CLI - Generating Unit Test...\n');

    let context = {
        project: cli_context.project,
        file: file,
        function_name: target.name,
        test_case: test_case,
        chat_options: cli_config.config.chat_options.write_unit_test
    };

    let result,
        test_code;

    try {
        result = await jobs.implement_unit_test(context);
        test_code = code_utils.getCodeBlockContent(result.test_code, 'it');
    } catch (err) {
        console.log('Error generating Unit Test...');
        await console_menu.waitForKeyPress();
        return;
    }

    console.log("\nGenerated Unit Test:\n\n" + test_code + "\n");

    console.log('Test is passing: ' + result.passing_test);

    let action_list = [
        { title: 'Copy Unit Test to Clipboard', action: 'copy' },
        { title: 'Regenerate Unit Test', action: 'regenerate' },
        { title: 'Return', action: 'return' }
    ];

    let action = await console_menu.getActionMenu(action_list);

    if (action === 'return') {
        return;
    } else if (action === 'regenerate') {
        return self(_.assign({}, cli_context, { unit_test_target: target }));
    } else if (action === 'copy') {
        await utils.copyToClipboard(test_code);
    }

}

async function getTarget(cli_context) {
    if (cli_context.unit_test_target) {
        return cli_context.unit_test_target;
    } else {
        return await select_target(cli_context, {
            functions: true,
            // getTargetColor: (type, target) => {
            //     return target.js_doc ? 'green' : 'red';
            // }
        });
    }
}

module.exports = self;