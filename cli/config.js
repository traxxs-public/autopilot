const fse = require('fs-extra'),
    _ = require('lodash');

const launch_path = process.cwd();

const config_file_path = '.autopilot/config.json';

const self = {

    config: {
        test_framework: 'jest',
        project_path: '',
        entry_point: '',
        file: '',
        unit_test_output_path: 'test/unit',
        text_editor: 'gedit',
        chat_options: {
            js_doc: {
                provider: 'groq',
                model: 'llama3-70b-8192'
            },
            write_unit_test: {
                provider: 'groq',
                model: 'llama3-8b-8192'
            },
            generate_unit_test_cases: {
                provider: 'groq',
                model: 'llama3-70b-8192'
            }
        }
    },

    getConfigPath: () => {
        return launch_path + '/' + config_file_path;
    },

    configExists: () => {
        return fse.existsSync(self.getConfigPath());
    },

    load: () => {
        let config = fse.readJsonSync(self.getConfigPath());
        _.assign(self.config, config);
        self.saveConfig();
    },

    set: (data) => {
        _.assign(self.config, data);
        self.saveConfig();
    },

    saveConfig: () => {
        fse.outputJsonSync(self.getConfigPath(), self.config, { spaces: 2 });
    }

}

module.exports = self;