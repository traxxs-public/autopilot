const Project = require('../parsing/project'),
    { jobs, pipe } = require('../autopilot'),
    utils = require('../lib/utils'),
    token_utils = require('../lib/ai/token_utils'),
    fse = require('fs-extra'),
    chalk = require('chalk'),
    path = require('path'),
    _ = require('lodash');

const config = {
    unit_test_output_path: 'test/unit',
    project_path: path.join(__dirname, '../../side-flip'),
    entry_point: 'index.js',
    target_file_path_list: [
        'utils/objects.js',
    ],
    chat_options: {
        write_unit_test: {
            provider: 'groq',
            model: 'llama3-70b-8192'
        },
        generate_unit_test_cases: {
            provider: "openai",
            model: "gpt-4o"
        }
    }
}

function getUnitTestsOutputPath(project, file, function_name, absolute = true) {
    let project_output_path = utils.computeFunctionTargetPath(config.unit_test_output_path, file, function_name, '.test.js');
    if (absolute) {
        return project.root_path + '/' + project_output_path;
    } else {
        return project_output_path;
    }
}

async function getTestCases(project, file, function_name) {
    console.log('\nGenerating Unit Test cases...');
    let context = {
        project: project,
        file: file,
        function_name: function_name,
        chat_options: config.chat_options.generate_unit_test_cases
    };
    let result = await pipe([
        jobs.extract_module_function.set({
            include_dependencies: true,
            max_depth: 0,
            only_prototypes: true
        }),
        jobs.generate_unit_test_cases
    ])(context);
    let test_cases = result.unit_test_cases;
    console.log('\nTest cases generated: ' + test_cases.length);
    return test_cases;
}

async function processFunction(project, file, function_name) {
    console.log('\nProcessing function: ' + function_name);
    let unit_test_cases = await getTestCases(project, file, function_name);

    console.log('\nGenerating Unit Tests...');

    let context = {
        project: project,
        file: file,
        function_name: function_name,
        unit_test_cases,
        chat_options: config.chat_options.write_unit_test,
        skip_failed_tests: true
    };

    let result;

    try {
        result = await jobs.implement_unit_test_suite(context);
    } catch (err) {
        console.log('Error generating Unit Tests...');
        console.log(err);
        return;
    }

    let test_code = result.test_code;

    console.log('\nGenerated Unit Tests:\n');

    for (let i = 0; i < unit_test_cases.length; i++) {
        let passing = result.passing_test_list[i],
            status = passing ? 'Pass' : 'Fail',
            color = passing ? 'green' : 'red';
        console.log((i + 1) + '. ' + unit_test_cases[i] + ": " + chalk[color](status));
    }

    let unit_test_output_path = project.root_path + '/' + getUnitTestsOutputPath(project, file, function_name, false);
    console.log('\nWriting unit tests to: ' + unit_test_output_path);
    fse.outputFileSync(unit_test_output_path, test_code);
}

async function processFile(project, file_path) {
    console.log('\nProcessing file: ' + file_path);
    let file = project.getFile(file_path),
        function_name_list = Object.keys(file.function_map),
        target_functions = [];
    for (let function_name of function_name_list) {
        let output_path = getUnitTestsOutputPath(project, file, function_name);
        if (!fse.existsSync(output_path)) {
            target_functions.push(function_name);
        }
    }
    console.log('Missing unit tests:');
    for (let function_name of target_functions) {
        console.log("- " + function_name);
    }

    for (let function_name of target_functions) {
        await processFunction(project, file, function_name);
    }

    console.log('\nAll missing unit tests generated.');
}

async function run() {
    console.log('Generating missing unit tests...\n');
    let project = new Project("my_project", config.project_path);
    project.load(config.entry_point);

    for (let file_path of config.target_file_path_list) {
        await processFile(project, file_path);
    }

    token_utils.printSessionTokenUsage();

    process.exit();
}

run();