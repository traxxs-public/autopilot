const fs = require('fs'),
    path = require('path'),
    glob = require('glob'),
    token_utils = require('../lib/ai/token_utils');

const PROJECT_PATH = path.resolve(__dirname, '..');

const OUTPUT_NAME = 'output/autopilot.txt';

let EXCLUDED_PATHS = [
    'node_modules',
    'test',
    'correlator_tools.js',
    'dev.js',
    'log',
    'benchmark',
    'examples',
    'configs',
    'scripts',
    '.autopilot'
];

EXCLUDED_PATHS = EXCLUDED_PATHS.map(p => path.relative(PROJECT_PATH, path.resolve(PROJECT_PATH, p)));

function discoverFiles(dir, exclude = [], mask = '*') {
    let results = [];

    function readDirRecursive(currentDir) {
        const files = fs.readdirSync(currentDir);
        for (const file of files) {
            const fullPath = path.join(currentDir, file);
            const relativePath = path.relative(dir, fullPath);
            if (exclude.includes(relativePath)) continue;
            const stat = fs.statSync(fullPath);
            if (stat && stat.isDirectory()) {
                readDirRecursive(fullPath);
            } else {
                if (glob.sync(mask, { cwd: path.dirname(fullPath) }).includes(file)) {
                    results.push(relativePath);
                }
            }
        }
    }

    readDirRecursive(dir);
    results.sort();
    return results;
}

function getCompiledFiles(path_list){
    let compiled = "";
    for(let file_path of path_list){
        let file = fs.readFileSync(path.join(PROJECT_PATH, file_path), 'utf8');
        compiled += "File: " + file_path + "\n";
        compiled += "```javascript\n"
        compiled += file;
        compiled += "\n```\n\n"
    }
    return compiled;
}
 
const discoveredFiles = discoverFiles(PROJECT_PATH, EXCLUDED_PATHS, '*.js');

const compiledFiles = getCompiledFiles(discoveredFiles);

fs.writeFileSync(OUTPUT_NAME, compiledFiles);

let token_count = token_utils.getTokenCount(compiledFiles);

console.log(`Token count: ${token_count}`);