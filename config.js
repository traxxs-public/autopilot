const fse = require('fs-extra'),
    path = require('path'),
    _ = require('lodash');

const CONFIG_PATH = path.join(__dirname, '.config');

let config_override;

config_override = require('./configs/side-flip.config');

const self = {

    test_framework: 'jest',

    log: {
        enabled: true,
        level: 1,
        log_file: true,
        path: 'log'
    },

    openai: {
        api_key: process.env.OPENAI_API_KEY,
        completion_max_concurrent: 30,
        completion_start_delay: 200,
        request_timeout: 60000
    },

    deepseek: {
        api_key: process.env.DEEPSEEK_API_KEY,
        request_timeout: 60000
    },

    groq: {
        api_key: process.env.GROQ_API_KEY,
        request_timeout: 60000
    },

    local_llm: {
        url: 'http://localhost:8000',
        access_token: 'frdqgqdsgdsfvxcvbwxwfdshfdpoqqdfijh'
    },
    
    project: {
        unit_test_output_path: 'test/unit',
        test_run_file_path: 'test/autopilot/tmp/tmp.test.js',
    },

    llm_providers: {

        openai: {
            models: {
                'gpt-3.5-turbo': {
                    target: 'gpt-3.5-turbo',
                    encoding: 'cl100k_base',
                    pricing: {
                        input: 0.0005,
                        output: 0.0015
                    },
                    max_total_tokens: 16000,
                    max_output_tokens: 2048,
                    rate_limits: {
                        tpm: 100000, rpm: 500, rpd: 10000
                    }
                },
                'gpt-4-turbo': {
                    target: 'gpt-4-turbo',
                    encoding: 'cl100k_base',
                    pricing: {
                        input: 0.01,
                        output: 0.03
                    },
                    max_total_tokens: 128000,
                    max_output_tokens: 4096,
                    rate_limits: {
                        tpm: 100000, rpm: 500, rpd: 10000
                    }
                },
                'gpt-4o': {
                    target: 'gpt-4o',
                    encoding: 'cl100k_base',
                    pricing: {
                        input: 0.005,
                        output: 0.015
                    },
                    max_total_tokens: 128000,
                    max_output_tokens: 4096,
                    rate_limits: {
                        tpm: 100000, rpm: 500, rpd: 10000
                    }
                }
            }
        },
        deepseek: {
            models: {
                'deepseek-chat': {
                    target: 'deepseek-chat',
                    encoding: 'cl100k_base',
                    pricing: {
                        input: 0.00014,
                        output: 0.00028
                    },
                    max_total_tokens: 32000,
                    max_output_tokens: 2048,
                    rate_limits: {
                        tpm: 100000, rpm: 500, rpd: 10000
                    }
                
                },
                'deepseek-coder': {
                    target: 'deepseek-coder',
                    encoding: 'cl100k_base',
                    pricing: {
                        input: 0.00014,
                        output: 0.00028
                    },
                    max_total_tokens: 16000,
                    max_output_tokens: 2048,
                    rate_limits: {
                        tpm: 100000, rpm: 500, rpd: 10000
                    }
                
                }
            }
        },
        groq: {
            models: {
                'llama3-8b-8192': {
                    target: 'llama3-8b-8192',
                    encoding: 'cl100k_base',
                    pricing: {
                        input: 0.00005,
                        output: 0.0001
                    },
                    max_total_tokens: 8192,
                    max_output_tokens: 2048,
                    rate_limits: {
                        tpm: 30000, rpm: 30, rpd: 14400
                    }
                },
                'llama3-70b-8192': {
                    target: 'llama3-70b-8192',
                    encoding: 'cl100k_base',
                    pricing: {
                        input: 0.00059,
                        output: 0.00079
                    },
                    max_total_tokens: 8192,
                    max_output_tokens: 2048,
                    rate_limits: {
                        tpm: 6000, rpm: 30, rpd: 14400
                    }
                },
                'gemma-7b-it': {
                    target: 'gemma-7b-it',
                    encoding: 'cl100k_base',
                    pricing: {
                        input: 0.0001,
                        output: 0.0001
                    },
                    max_total_tokens: 8192,
                    max_output_tokens: 2048,
                    rate_limits: {
                        tpm: 15000, rpm: 30, rpd: 14400
                    }
                },
                'mixtral-8x7b-32768': {
                    target: 'mixtral-8x7b-32768',
                    encoding: 'cl100k_base',
                    pricing: {
                        input: 0.00027,
                        output: 0.00027
                    },
                    max_total_tokens: 32768,
                    max_output_tokens: 2048,
                    rate_limits: {
                        tpm: 5000, rpm: 30, rpd: 14400
                    }
                }
            }
        }
        
    },

    disable_parsing_file_list: [],

    dependencies_filter: {
        
    },

    dependencies_only_prototype: {

    },

    include_external_dependencies: [],

    readConfig: () => {
        let config = {};
        try{
            let data = fse.readFileSync(CONFIG_PATH, 'utf8'),
            lines = data.split('\n');
        for(let line of lines){
            let parts = line.split('=');
            if(parts.length === 2){
                let key = parts[0].trim(),
                    value = parts[1].trim();
                config[key] = value;
            }
        }
        }catch(err){

        }
        return config;
    },

    writeConfig: (config) => {
        let data = '';
        for(let key in config){
            data += key + '=' + config[key] + '\n';
        }
        fse.writeFileSync(CONFIG_PATH, data, 'utf8');
    },

    reloadConfig: () => {
        let config = self.readConfig();
        Object.assign(self, config);
    },

    saveConfig: () => {
        let config = self.readConfig();
        for(let key in config){
            config[key] = self[key];
        }
        self.writeConfig(config);
    },

    setConfig: (key, value) => {
        self[key] = value;
        self.saveConfig();
    }

}

self.reloadConfig();

if(config_override){
    _.merge(self, config_override);
}

module.exports = self;