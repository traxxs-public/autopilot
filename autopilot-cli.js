#!/usr/bin/env node

const Project = require('./parsing/project'),
    cli_config = require('./cli/config'),
    console_menu = require('./lib/console_menu'),
    main_menu = require('./cli/main_menu');

const launch_path = process.cwd();

async function setup(){
    console.log('Setting up project...');
    await console_menu.waitForKeyPress();
    console.clear();
    let project_path = await console_menu.waitForInput('Enter the path of the project: ', launch_path);
    let entry_point = await console_menu.waitForInput('Enter the entry point of the project: ', 'index.js');
    cli_config.set({project_path: project_path, entry_point: entry_point});
}

async function run() {
    console.log('Welcome to the Autopilot CLI\n');

    if(!cli_config.configExists()){
        await setup();
    }else{
        cli_config.load();
    }
    let project_path = cli_config.config.project_path;
    console.log('Loading project: ' + project_path);
    let project = new Project("my_project", project_path);
    
    project.load(cli_config.config.entry_point);
    let cli_context = {
        project: project
    }
    if(cli_config.config.file){
        let file = project.getFile(cli_config.config.file);
        if(file){
            console.log('Target file loaded: ' + file.project_path);
            cli_context.file = file;
        }
    }

    console.log('Project loaded.\n');
    // await console_menu.waitForKeyPress();

    await main_menu(cli_context);

    process.exit();
}

run();