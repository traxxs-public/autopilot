const self = {

    project: {
        unit_test_output_path: 'test/autopilot/unit',
        test_run_file_path: 'test/autopilot/tmp/tmp.test.js',
    },

    dependencies_filter: {
        'foo/bar.js': true,
        'foo/foo.js': [
            'fn',
            'var'
        ],
        'node_modules/foo/bar2.js': true,
        'node_modules/foo/bar3.js': [
            'var'
        ]
    },

    // Include external dependencies that match the filter (node_modules)
    include_external_dependencies: [
        {path: {_startsWith: '@alextis59/side-flip'}}
    ],

    disable_parsing_file_list: [
        'bar.js',
        'node_modules/foo/bar.js'
    ]

}

module.exports = self;