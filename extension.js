// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require('vscode'),
    AutoPilotViewProvider = require('./vscode_plugin/views/AutoPilotViewProvider'),
    generate_code = require('./vscode_plugin/bin/generate_code'),
    modify_code = require('./vscode_plugin/bin/modify_code'),
    generate_target_js_doc = require('./vscode_plugin/bin/generate_target_js_doc'),
    generate_unit_test_suite = require('./vscode_plugin/bin/generate_unit_test_suite');

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed

const commands = [
    {
        command: "autopilot.generateCode",
        fn: generate_code
    },
    {
        command: "autopilot.modifyCode",
        fn: modify_code
    },
    {
        command: "autopilot.generateJSDoc",
        fn: generate_target_js_doc
    },
    {
        command: "autopilot.generateUnitTests",
        fn: generate_unit_test_suite
    }
]

/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {

	console.log('Activating AutoPilot extension...');

    commands.forEach(({command, fn}) => {
        let disposable = vscode.commands.registerCommand(command, fn);
        context.subscriptions.push(disposable);
    });

    const provider = new AutoPilotViewProvider(context.extensionUri);

    try{
        console.log('Registering AutoPilotViewProvider');
        context.subscriptions.push(
            vscode.window.registerWebviewViewProvider(AutoPilotViewProvider.viewType, provider)
        );
    }catch(err){
        console.log(err);
    }

    console.log('AutoPilot extension is now active!');

}

// This method is called when your extension is deactivated
function deactivate() {}

module.exports = {
	activate,
	deactivate
}
