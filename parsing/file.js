const _ = require('lodash'),
    utils = require('../lib/utils'),
    fse = require('fs-extra');

class File {

    name = '';

    module_name = '';

    path = '';

    project_path = '';

    requested_path_list = [];

    path_without_filename = '';

    absolute_path = '';

    content = '';

    libs = [];

    disable_parsing = false;

    parsed = false;

    dependencies_parsed = false;

    parent_dependencies_parsed = false;

    function_map = {};

    variable_map = {};

    constructor(root_path, path, requested_path, disable_parsing) {
        let absolute_path = root_path + '/' + path,
            content = fse.readFileSync(absolute_path, 'utf8'),
            filename = absolute_path.split('/').pop(),
            file_prefix = filename.replace('.js', ''),
            module_name = file_prefix === 'index' ? path.split('/')[path.split('/').length - 2] : file_prefix;

        let requested_path_list = [path];
        if (requested_path && requested_path !== path) {
            requested_path_list.push(requested_path);
        }
        this.name = filename;
        this.module_name = module_name;
        this.path = path;
        this.project_path = path;
        this.requested_path_list = requested_path_list;
        if(path.includes('/')){
            this.path_without_filename = path.replace('/' + filename, '');
        }else{
            this.path_without_filename = '';
        }
        this.absolute_path = absolute_path;
        this.content = content;
        this.disable_parsing = disable_parsing || false;
    }

    save = () => {
        fse.outputFileSync(this.absolute_path, this.content);
    };

    replace = (search, replacement, save = true) => {
        this.content = this.content.replace(search, replacement);
        if(save){
            this.save();
        }
    };

    insertBeforeLineIndex = (line_index, content, save = true, copy_indent = true) => {
        this.content = utils.insertBeforeLineIndex(this.content, line_index, content, copy_indent);
        if(save){
            this.save();
        }
    };

    insertBeforeLine = (search, content, save = true, copy_indent = true) => {
        this.content = utils.insertBeforeLine(this.content, search, content, copy_indent);
        if(save){
            this.save();
        }
    };

    getTargetFromLineIndex = (line_index) => {
        for(let function_name in this.function_map){
            let target = this.function_map[function_name],
                line_indexes = target.lines_indexes;
            if(line_indexes && line_index >= line_indexes.start && line_index <= line_indexes.end){
                return {name: function_name, type: 'function'};
            }
        }
        for(let variable_name in this.variable_map){
            let target = this.variable_map[variable_name],
                line_indexes = target.lines_indexes;
            if(line_indexes && line_index >= line_indexes.start && line_index <= line_indexes.end){
                return {name: variable_name, type: 'variable'};
            }
        }
    };

    getTargetExtract = (type, name) => {
        let target_map = this[`${type}_map`];
        if(!target_map){
            return null;
        }
        let target = target_map[name],
            line_indexes = target && target.lines_indexes;
        if(!line_indexes){
            return null;
        }
        return {
            start: line_indexes.start,
            end: line_indexes.end,
            content: utils.getFileLinesFromIndexes(this.content, line_indexes.start, line_indexes.end)
        }
    };

    replaceLines = (start, end, content) => {
        this.replace(utils.getFileLinesFromIndexes(this.content, start, end), content);
    }

}

module.exports = File;