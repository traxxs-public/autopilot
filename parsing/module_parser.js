const _ = require('lodash'),
    config = require('../config'),
    utils = require('../lib/utils'),
    code_utils = require('../lib/code_utils'),
    esprima = require('esprima-next');

function extractRequires(file) {
    try{
        let code = file.content;
        const ast = esprima.parseScript(code, { comment: true });
        const requires = ast.body
            .filter(node => node.type === 'VariableDeclaration')
            .flatMap(declaration => declaration.declarations)
            .filter(declarator => declarator.init && declarator.init.type === 'CallExpression' && declarator.init.callee.name === 'require');
        let lib_index = 0;
        return requires.flatMap(req => {
            const path = req.init.arguments[0].value;
            if (req.id.type === 'Identifier') {
                let lib = {
                    name: req.id.name,
                    path: path,
                    local: path.startsWith('.'),
                    path_placeholder: 'LIB_' + lib_index++
                };
                if (lib.local) {
                    lib.project_path = utils.recomputePath(path, file.project_path);
                } else if (self.includeExternalLibrary(lib)) {
                    lib.local = true;
                    lib.external = true;
                    lib.project_path = 'node_modules/' + path;
                }
                return [lib];
            } else if (req.id.type === 'ObjectPattern') {
                let lib = {
                    object: true,
                    object_list: [],
                    path: path,
                    local: path.startsWith('.'),
                    path_placeholder: 'LIB_' + lib_index++
                }
                // lib.name = lib.path.split('/').pop().replace('.js', '');
                if (lib.local) {
                    lib.project_path = utils.recomputePath(path, file.project_path);
                } else if (self.includeExternalLibrary(lib)) {
                    lib.local = true;
                    lib.external = true;
                    lib.project_path = 'node_modules/' + path;
                }
                for (let prop of req.id.properties) {
                    lib.object_list.push(prop.key.name);
                }
                return [lib];
            }
        });
    }catch(err){
        console.log('Error while extracting requires for file: ' + file.project_path);
        console.log(err);
        process.exit(1);
    }
}

function getExportedVariableName(content, path) {
    const ast = esprima.parseScript(content, { range: true });

    const assignmentExpressions = ast.body.filter(node => {
        return node.type === 'ExpressionStatement' &&
            node.expression.type === 'AssignmentExpression';
    });

    const moduleExports = assignmentExpressions.find(node =>
        node.expression.left.object && node.expression.left.object.name === 'module' &&
        node.expression.left.property && node.expression.left.property.name === 'exports'
    );

    if (!moduleExports) {
        console.log('Module export not found in file: ' + path);
        // process.exit(1);
        return undefined;
    }

    if (moduleExports.expression.right.type === 'Identifier') {
        return moduleExports.expression.right.name;
    } else if (moduleExports.expression.right.type === 'ObjectExpression') {
        return 'self';
    } else if (moduleExports.expression.right.type === 'FunctionExpression') {
        return undefined;
    } else {
        //console.log(`Unexpected type of module exports in file`);
        return undefined;
    }
}

const lib_usage_search_suffix_list = [' ', '(', '[', ';', ',', '\n', '\t', '=', '!'];

function findLibUsageIndex(code, target, search_index) {
    let index = code.indexOf(target, search_index);
    if (index === -1) {
        return null;
    } else {
        let previous_char = code[index - 1] || '',
            next_char = code[index + target.length] || '';
        if (lib_usage_search_suffix_list.indexOf(previous_char) === -1 || (next_char !== '.' && next_char !== '(')) {
            return findLibUsageIndex(code, target, index + 1);
        }
        return {
            index: index,
            type: next_char === '.' ? 'attr' : 'call'
        }
    }
}

const attr_regex = /\.(\w+)[\(\)\n\t=;.,"' \[\]]/;

function computeFunctionLibDependencies(code, lib) {
    let dependencies = [],
        lib_targets = lib.object ? lib.object_list : [lib.name];
    for (let target of lib_targets) {
        if(target === '$checkPoint$'){
            continue;
        }
        let search_index = 0,
            next_index;
        while ((next_index = findLibUsageIndex(code, target, search_index)) !== null) {
            try {
                if (next_index.type === 'attr') {
                    let index = next_index.index,
                        following_code = code.substr(index),
                        lib_attr_match = following_code.match(attr_regex),
                        lib_attr = lib_attr_match ? lib_attr_match[1] : undefined,
                        next_char = lib_attr ? following_code[target.length + 1 + lib_attr.length] : undefined,
                        corresponding_line = utils.findLineByCharIndex(code, index);
                    if (lib_attr && lib_attr.length < 50 && following_code.indexOf(lib_attr) === (target.length + 1) 
                        && ["'", '"'].indexOf(next_char) === -1 && !utils.isCommentLine(corresponding_line)) {
                        let dependency = {
                            name: lib.name,
                            local: lib.local,
                            target: lib_attr,
                            type: 'unknown',
                            depth: 0
                        };
                        if (lib.local || lib.include) {
                            dependency.project_path = lib.project_path;
                        }
                        if (!_.find(dependencies, dependency)) {
                            dependencies.push(dependency);
                        }
                    }
                } else if (next_index.type === 'call'){
                    let dependency = {
                        name: lib.name,
                        local: lib.local,
                        target: lib.object ? target : ".",
                        type: 'function',
                        depth: 0
                    };
                    if (lib.local || lib.include) {
                        dependency.project_path = lib.project_path;
                    }
                    if (!_.find(dependencies, dependency)) {
                        dependencies.push(dependency);
                    }
                }
                search_index = next_index.index + 1;
            } catch (e) {
                console.log('Error while parsing lib attr for lib target: ' + target);
                console.log(e);
                process.exit(1);
            }
        }
    }
    return dependencies;
}

function includeDependency(lib) {
    if (lib.local) {
        return true;
    } else {
        return false;
    }
}

function computeFunctionDependencies(function_code, libs) {
    let dependencies = [];
    for (let lib of libs) {
        if (includeDependency(lib)) {
            let lib_dependencies = computeFunctionLibDependencies(function_code, lib);
            for (let dependency of lib_dependencies) {
                if (!_.find(dependencies, dependency)) {
                    dependencies.push(dependency);
                }
            }
        }
    }
    return _.uniq(dependencies);
}

function extractModuleExportJsDoc(code, export_name) {
    try {
        let lines = code.split('\n'),
            js_doc_lines = [],
            export_line = -1;
        for (let i = 0; i < lines.length; i++) {
            let line = lines[i];
            if (line.indexOf(" " + export_name + ':') > -1 || line.indexOf(" " + export_name + ' :') > -1 ||
                line.indexOf("\t" + export_name + ':') > -1 || line.indexOf("\t" + export_name + ' :') > -1) {
                export_line = i;
                break;
            }
        }
        if (export_line > -1) {
            for (let i = export_line - 1; i >= 0; i--) {
                let line = lines[i];
                if (line.trim().indexOf('//') === 0 || line.trim().indexOf('*') === 0
                    || line.trim().indexOf('/*') === 0 || line.trim().indexOf('*/') === 0) {
                    js_doc_lines.unshift(line);
                } else {
                    break;
                }
            }
        }
        if (js_doc_lines.length > 0) {
            return js_doc_lines.join('\n');
        } else {
            return undefined;
        }
    } catch (e) {
        console.log('Error while extracting js doc for export: ' + export_name);
        console.log(e);
        process.exit(1);
    }
}

function getVariableValueString(value) {
    let result = '';
    try {
        result = JSON.stringify(value)
        if (_.isFunction(value)) {
            result = value.name || 'anonymous function';
        } else if (_.isArray(value)) {
            let values = [];
            for (let item of value) {
                values.push(getVariableValueString(item));
            }
            result = '[ ' + values.join(', ') + ' ]';
        } else if (_.isObject(value)) {
            let values = [];
            for (let key in value) {
                values.push(key + ': ' + getVariableValueString(value[key]));
            }
            result = '{ ' + values.join(', ') + ' }';
        }
    } catch (err) {
        if (err.message.includes('Converting circular structure to JSON')) {
            if (_.isObject(value)) {
                result = '{ ' + Object.keys(value).join(', ') + ' }';
            } else {
                result = value.toString ? value.toString() : 'Circular structure';
            }
        }
    }
    return result;
}

function findVariableDeclarationString(code, object_name, variable_name) {
    try {
        const ast = esprima.parseScript(code, { range: true, tokens: true, comment: true });

        for (const declaration of ast.body) {
            if (declaration.type === 'VariableDeclaration') {
                for (const declarator of declaration.declarations) {
                    if (declarator.id.name === object_name) {
                        if (declarator.init.type === 'ObjectExpression') {
                            for (const property of declarator.init.properties) {
                                if (property.key.name === variable_name) {
                                    const propertyStart = property.range[0];
                                    const propertyEnd = property.range[1];

                                    // Retrieve the range of source code corresponding to the property
                                    return code.slice(propertyStart, propertyEnd);
                                }
                            }
                        }
                    }
                }
            }
        }
    } catch (err) {

    }
    return null;
}

function parseInternalFunctions(code) {
    let function_map = {};
    try {
        const ast = esprima.parseScript(code, { range: true, tokens: true, comment: true });
        for (const declaration of ast.body) {
            if (declaration.type === 'FunctionDeclaration') {
                function_map[declaration.id.name] = {
                    name: declaration.id.name,
                    code: code.slice(declaration.range[0], declaration.range[1]),
                    dependencies: [],
                    js_doc: ''
                };
            }
        }
    } catch (err) {
        console.log('Error while parsing internal functions');
        console.log(err);
    }
    return function_map;
}

function parseInternalVariables(code, export_name) {
    let variable_map = {};
    try {
        const ast = esprima.parseScript(code, { range: true, tokens: true, comment: true });
        for (const declaration of ast.body) {
            if (declaration.type === 'VariableDeclaration') {
                for (const declarator of declaration.declarations) {
                    if (declarator.id.type === 'Identifier') {
                        let var_code = code.slice(declarator.range[0], declarator.range[1]);
                        if (!var_code.includes('require') && declarator.id.name !== export_name) {
                            variable_map[declarator.id.name] = {
                                name: declarator.id.name,
                                code: code.slice(declarator.range[0], declarator.range[1]),
                                dependencies: [],
                                js_doc: ''
                            };
                        }
                    }
                }
            }
        }
    } catch (err) {
        console.log('Error while parsing internal variables');
        console.log(err);
    }
    return variable_map;
}

function computeVariableIndent(file_content, variable_name) {
    let variable_line = utils.getFirstMatchingLine(file_content, variable_name + ':') || utils.getFirstMatchingLine(file_content, variable_name + ' :');
    return code_utils.getLineIndent(variable_line);
}

function computeCodeIndent(file_content, code) {
    let code_lines = code.split('\n'),
        code_line = code_lines[0],
        file_code_line = utils.getFirstMatchingLine(file_content, code_line);
    return code_utils.getLineIndent(file_code_line);
}

let parsing_error_path_list = [],
    unexpected_export_name_list = [],
    function_export_warning_list = [];

const self = {

    extractRequires: extractRequires,

    includeExternalLibrary: (lib) => {
        let included_external_depencies = config.include_external_dependencies || [];
        for (let dependency of included_external_depencies) {
            if (dependency.path) {
                if (dependency.path._startsWith && _.startsWith(lib.path, dependency.path._startsWith)) {
                    return true;
                } else if (dependency.path === lib.path) {
                    return true;
                }
            }
        }
    },

    parseModule: (file) => {
        delete require.cache[require.resolve(file.absolute_path)];
        let module = require(file.absolute_path),
            function_map = {},
            internal_function_map = {},
            variable_map = {},
            internal_variable_map = {};
        try {
            let export_name = getExportedVariableName(file.content, file.absolute_path),
                export_type = typeof module;
            if (!export_name) {
                if (unexpected_export_name_list.indexOf(file.absolute_path) === -1) {
                    // console.log(`Unexpected type of module exports in file: ` + file.name);
                    unexpected_export_name_list.push(file.absolute_path);
                }
            }
            file.export_name = export_name || file.module_name;
            file.export_type = export_type;
            if (export_type === 'function') {
                if (function_export_warning_list.indexOf(file.absolute_path) === -1) {
                    console.log(`Function export in file: ` + file.absolute_path);
                    function_export_warning_list.push(file.absolute_path);
                }
            }
            let dependencies_libs = _.cloneDeep(file.libs);
            if(export_name){
                dependencies_libs.push({
                    name: file.export_name,
                    project_path: file.project_path,
                    local: true
                });
            }
            if (file.export_name) {
                for (let key in module) {
                    // Verify that the key come from the module and not from another file
                    let external_source = false;
                    if (typeof module[key] === 'function') {
                        let code = module[key].toString();
                        external_source = file.content.indexOf(code) === -1;
                        function_map[key] = {
                            name: key,
                            code: code,
                            indent: computeCodeIndent(file.content, code) || "",
                            dependencies: external_source ? [] : computeFunctionDependencies(code, dependencies_libs),
                            js_doc: external_source ? undefined : extractModuleExportJsDoc(file.content, key),
                            external_source
                        };
                    } else {
                        let var_code = findVariableDeclarationString(file.content, file.export_name, key) || getVariableValueString(module[key]),
                            code_include_declaration = var_code && var_code.indexOf(key + ':') === 0;
                        if (!module.hasOwnProperty(key) || (file.content.indexOf(key + ':') === -1 && file.content.indexOf(key + ' :') === -1)) {
                            external_source = true;
                        }
                        variable_map[key] = {
                            name: key,
                            code: var_code,
                            indent: computeVariableIndent(file.content, key) || "",
                            dependencies: [],
                            js_doc: external_source ? undefined : extractModuleExportJsDoc(file.content, key),
                            code_include_declaration,
                            external_source
                        };
                    }
                }
                if(export_type === 'function'){
                    let code = module.toString();
                    function_map['.'] = {
                        name: '.',
                        code: code,
                        indent: computeCodeIndent(file.content, code) || "",
                        dependencies: computeFunctionDependencies(code, dependencies_libs),
                        // js_doc: extractModuleExportJsDoc(file.content, export_name),
                        js_doc: "",
                        external_source: false
                    };
                }
            }
            internal_function_map = parseInternalFunctions(file.content);
            internal_variable_map = parseInternalVariables(file.content, export_name);
        } catch (e) {
            if (parsing_error_path_list.indexOf(file.absolute_path) === -1) {
                console.log('Error while parsing module: ' + file.absolute_path);
                console.log(e);
                parsing_error_path_list.push(file.absolute_path);
            }
            file.export_name = file.export_name || file.module_name;
        }
        file.function_map = function_map;
        file.internal_function_map = internal_function_map;
        file.variable_map = variable_map;
        file.internal_variable_map = internal_variable_map;
        self.computeFileLinesIndexes(file);
    },

    computeFileLinesIndexes: (file) => {
        let content = file.content;
        for(let function_name in file.function_map){
            let target = file.function_map[function_name];
            target.lines_indexes = self.computeTargetLinesIndexes(content, target.code);
            if(target.lines_indexes && target.js_doc){
                let js_doc_indexes = self.computeTargetLinesIndexes(content, target.js_doc);
                if(js_doc_indexes && js_doc_indexes.start < target.lines_indexes.start){
                    target.lines_indexes.start = js_doc_indexes.start;
                }
            }
        }
        for(let variable_name in file.variable_map){
            let target = file.variable_map[variable_name];
            target.lines_indexes = self.computeTargetLinesIndexes(content, target.code);
            if(target.lines_indexes && target.js_doc){
                let js_doc_indexes = self.computeTargetLinesIndexes(content, target.js_doc);
                if(js_doc_indexes && js_doc_indexes.start < target.lines_indexes.start){
                    target.lines_indexes.start = js_doc_indexes.start;
                }
            }
        }
    },

    computeTargetLinesIndexes: (content, search) => {
        let start_line = utils.getLineNumberFromMatch(content, search);
        if(start_line === -1){
            return null;
        }
        let line_count = search.split('\n').length;
        return {
            start: start_line,
            end: start_line + line_count - 1
        }
    },

    getModuleFunctionDependencies: (project, project_path, function_name, dependency_map = {}) => {
        let file = project.getFile(project_path),
            target_function = file.function_map[function_name];
        for (let dependency of target_function.dependencies) {
            let dependency_file = project.getFile(dependency.project_path),
                dependency_filter = config.dependencies_filter[dependency_file.project_path];
            if (dependency_filter) {
                if (dependency_filter === true) {
                    continue;
                }
                let filter_out = false;
                for (let filter of dependency_filter) {
                    if (dependency.name === filter) {
                        filter_out = true;
                        break;
                    }
                }
                if (filter_out) {
                    continue;
                }
            }
            dependency_map[dependency_file.project_path] = dependency_map[dependency_file.project_path] || {};
            if (dependency_map[dependency_file.project_path][dependency.name]) {
                continue;
            }
            if (dependency_file.function_map[dependency.name]) {
                dependency_map[dependency_file.project_path][dependency.name] = 'function';
                self.getModuleFunctionDependencies(project, dependency_file.project_path, dependency.name, dependency_map);
            } else if (dependency_file.variable_map[dependency.name]) {
                dependency_map[dependency_file.project_path][dependency.name] = 'variable';
            }
        }
        return dependency_map;
    },

    getModuleTargetDependenciesMap: (project, project_path, target_type, target_name, options = {}) => {
        let use_filter = options.use_filter || true,
            min_depth = options.min_depth || 0,
            max_depth = options.max_depth || 0,
            filters = config.dependencies_filter,
            file = project.getFile(project_path),
            dependencies = _.get(file, target_type + '_map.' + target_name + '.dependencies', []),
            map = {};
        for (let dependency of dependencies) {
            if (dependency.type !== 'unknown') {
                if (use_filter && (filters[dependency.project_path] === true ||
                    (filters[dependency.project_path] && filters[dependency.project_path].indexOf(dependency.target) > -1))) {
                    continue;
                } else if (dependency.depth > max_depth) {
                    continue;
                } else if (dependency.depth < min_depth) {
                    continue;
                }
                map[dependency.project_path] = map[dependency.project_path] || {};
                map[dependency.project_path][dependency.target] = dependency.type;
            }
        }
        return map;
    },

    getModuleFunctionDependenciesMap: (project, project_path, function_name, use_filter, max_depth) => {
        return self.getModuleTargetDependenciesMap(project, project_path, 'function', function_name, { use_filter, max_depth });
    }

}

module.exports = self;