const _ = require('lodash'),
    module_parser = require('./module_parser'),
    code_utils = require('../lib/code_utils');

const DEBUG = false;

function log(message) {
    if (DEBUG) {
        console.log('extract: ' + message);
    }
}

function shouldIncludeJsDoc(target_name, target, options = {}) {
    return target.js_doc && options.include_js_doc && (!options.exclude_js_doc_targets || options.exclude_js_doc_targets.indexOf(target_name) === -1);
}

function isTargetVariable(target_file_path, target_name, options = {}) {
    return options.target_file_path === target_file_path && options.target_variable === target_name;
}

function isTargetFunction(target_file_path, target_name, options = {}) {
    return options.target_file_path === target_file_path && options.target_function === target_name;
}

function shouldOnlyIncludePrototype(file, target_name, options = {}) {
    let target_file_path = file.absolute_path,
        target_code = file.function_map[target_name].code; 
    if(target_code && file.content.indexOf(target_code) === -1){
        return true;
    }
    return options.only_prototypes &&
        (!options.only_prototypes_exclude_target_function || !isTargetFunction(target_file_path, target_name, options));
}

const self = {

    extractModuleTarget: (project, file, target_type, target_name, options = {}) => {
        log('Extracting code from ' + file.name + ' for ' + target_type + ' ' + target_name);
        options = Object.assign(_.cloneDeep(options), { target_file_path: file.absolute_path });
        if (target_type === 'function') {
            options.target_function = target_name;
        } else if (target_type === 'variable') {
            options.target_variable = target_name;
        }
        if (options.include_dependencies) {
            return self.buildModuleTargetAndDependenciesExtract(project, file, target_type, target_name, options)
        }
        let function_list = [],
            variable_list = [];
        if (target_type === 'function') {
            function_list.push(target_name);
            let fn_dependencies = file.function_map[target_name].dependencies;
            for (let dependency of fn_dependencies) {
                if (dependency.depth === 0 && dependency.type === 'variable' && dependency.project_path === file.project_path) {
                    variable_list.push(dependency.target);
                }
            }
        } else if (target_type === 'variable') {
            variable_list.push(target_name);
        }
        return self.buildModuleExtract(project, file.project_path, function_list, variable_list, options);
    },

    extractModuleFunction: (project, file, function_name, options = {}) => {
        return self.extractModuleTarget(project, file, 'function', function_name, options);
    },

    buildVariableExtract: (file, variable_name, options = {}) => {
        let extract = "",
            indent = options.indent || '    ',
            target = file.variable_map[variable_name];
        if (!target) {
            console.error('Variable ' + variable_name + ' not found in ' + file.absolute_path);
            process.exit(1);
        }
        if (!options.without_js_doc && shouldIncludeJsDoc(variable_name, target, options)) {
            extract += target.js_doc + '\n';
        }
        if(options.without_prefix){
            extract += target.code;
        }else{
            if (target.code_include_declaration) {
                extract += indent + target.code;
            }else{
                extract += indent + variable_name + ': ' + target.code;
            }
        }
        return extract;
    },

    buildFunctionExtract: (file, function_name, options = {}) => {
        let extract = "",
            indent = options.indent || '    ',
            target = file.function_map[function_name];
        if (!target) {
            console.error('Function ' + function_name + ' not found in ' + file.absolute_path);
            process.exit(0);
        }
        if (!options.without_js_doc && shouldIncludeJsDoc(function_name, target, options)) {
            extract += target.js_doc + '\n';
        }
        let prefix = options.without_prefix ? "" : indent + function_name + ': ';
        if (shouldOnlyIncludePrototype(file, function_name, options)) {
            let param_list = code_utils.getFunctionParamList(file, function_name);
            extract += prefix + '(' + param_list.join(', ') + ')';
        } else {
            if (options.target_function_code_override && isTargetFunction(file.absolute_path, function_name, options)) {
                extract += prefix + options.target_function_code_override;
            } else {
                extract += prefix + target.code;
            }
        }
        return extract;
    },

    buildModuleExtract: (project, project_path, function_name_list = [], variable_name_list = [], options = {}) => {
        if(function_name_list.indexOf('.') > -1){
            return self.buildFunctionModuleExtract(project, project_path, function_name_list, variable_name_list, options);
        }
        let file = project.getFile(project_path),
            extract = 'const ' + file.export_name + ' = {\n\n';
        for (let variable_name of variable_name_list) {
            extract += self.buildVariableExtract(file, variable_name, options);
            extract += ',\n\n';
        }
        for (let i = 0; i < function_name_list.length; i++) {
            let function_name = function_name_list[i];
            extract += self.buildFunctionExtract(file, function_name, options);
            if (i < function_name_list.length - 1) {
                extract += ',\n\n';
            } else {
                extract += '\n\n';
            }
        }
        extract += '};\n\nmodule.exports = ' + file.export_name + ';';
        if (options.include_filename) {
            if (file.external_module) {
                extract = '// ' + file.path.replace('node_modules/', '') + ' (external module)\n' + extract;
            } else {
                extract = '// ' + file.module_name + '.js\n' + extract;
            }
        }
        return extract;
    },

    buildFunctionModuleExtract: (project, project_path, function_name_list, variable_name_list, options) => {
        let file = project.getFile(project_path),
            extract = 'const ' + file.export_name + ' = ';
        extract += self.buildFunctionExtract(file, '.', Object.assign({}, options, {without_js_doc: true, without_prefix: true})) + ';\n\n'
        for (let variable_name of variable_name_list) {
            extract += file.export_name + '.' + variable_name + " = ";
            extract += self.buildVariableExtract(file, variable_name, Object.assign({}, options, {without_prefix: true}));
            extract += ',\n\n';
        }
        for (let function_name of function_name_list) {
            if(function_name !== '.'){
                extract += file.export_name + '.' + function_name + " = ";
                extract += self.buildFunctionExtract(file, function_name, Object.assign({}, options, {without_prefix: true}));
                extract += ';\n\n';
            }
        }
        extract += 'module.exports = ' + file.export_name + ';';
        if (options.include_filename) {
            if (file.external_module) {
                extract = '// ' + file.path.replace('node_modules/', '') + ' (external module)\n' + extract;
            } else {
                extract = '// ' + file.module_name + '.js\n' + extract;
            }
        }
        return extract;
    },

    buildDependencyExtract: (project, dependency, path, options) => {
        let extract = "";
        let function_list = [],
            variable_list = [];
        for (let target in dependency) {
            if (dependency[target] === 'function') {
                function_list.push(target);
            } else {
                variable_list.push(target);
            }
        }
        if (function_list.length > 0 || variable_list.length > 0) {
            options = options ? _.cloneDeep(options) : {};
            options.include_filename = true;
            extract = self.buildModuleExtract(project, path, function_list, variable_list, options);
        }
        return extract;
    },

    buildModuleTargetAndDependenciesExtract: (project, file, target_type, target_name, options) => {
        let dependencies = module_parser.getModuleTargetDependenciesMap(project, file.project_path, target_type, target_name, options);
        dependencies[file.project_path] = dependencies[file.project_path] || {};
        dependencies[file.project_path][target_name] = target_type;
        let path_list = _.without(_.keys(dependencies), file.project_path);
        path_list.push(file.project_path);
        let file_extracts = [];
        for (let path of path_list) {
            file_extracts.push(self.buildDependencyExtract(project, dependencies[path], path, options));
        }
        return file_extracts.join('\n\n');
    },

    buildModuleFunctionAndDependenciesExtract: (project, file, function_name, options) => {
        return self.buildModuleTargetAndDependenciesExtract(project, file, 'function', function_name, { max_depth: options.max_depth });
    },

    extractTestFunction(content, name) {
        let start = content.indexOf(name ? 'it("' + name + '"' : 'it("'),
            function_opening_bracket = content.indexOf('{', start),
            stack = [],
            end;

        // Start from the opening bracket of the function
        for (let i = function_opening_bracket; i < content.length; i++) {
            if (content[i] === '{') {
                stack.push('{');
            } else if (content[i] === '}') {
                if (stack.length === 0) {
                    // Error: unbalanced brackets
                    return null;
                }
                stack.pop();
                if (stack.length === 0) {
                    // We've found the matching closing bracket
                    end = i;
                    break;
                }
            }
        }

        if (stack.length !== 0) {
            // Error: unbalanced brackets
            return null;
        }

        // Extract the function code
        let functionCode = content.slice(start, end + 3);

        return functionCode;
    }

}

module.exports = self;