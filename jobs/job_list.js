const GetChatCompletion = require('./ai/get_chat_completion'),

    ExtractModuleFunction = require('./code/extraction/extract_module_function'),
    ExtractModuleVariable = require('./code/extraction/extract_module_variable'),

    GetUnitTestTemplate = require('./code/unit_test/builders/get_unit_test_template'),

    RunUnitTest = require('./code/unit_test/execution/run_unit_test'),
    UnitTestSuccess = require('./code/unit_test/execution/unit_test_success'),

    GenerateCode = require('./code/generation/generate_code'),

    ImplementCode = require('./code/implementation/implement_code/implement_code'),
    ModifyCode = require('./code/implementation/modify_code/modify_code'),

    GenerateUnitTestCases = require('./code/unit_test/generation/generate_unit_test_cases/generate_unit_test_cases'),
    WriteUnitTestCode = require('./code/unit_test/generation/write_unit_test_code/write_unit_test_code'),
    SelectBestTestCode = require('./code/unit_test/evaluation/select_best_test_code'),

    ImplementUnitTest = require('./code/unit_test/strategies/implement_unit_test'),
    ImplementUnitTestSuite = require('./code/unit_test/strategies/implement_unit_test_suite'),

    GenerateFunctionJsDoc = require('./code/js_doc/generate_function_js_doc/generate_function_js_doc'),
    GenerateVariableJsDoc = require('./code/js_doc/generate_variable_js_doc/generate_variable_js_doc'),
    FillFunctionJsDoc = require('./code/js_doc/fill_function_js_doc'),

    IsValidCode = require('./utils/is_valid_code'),
    IsValidJsDoc = require('./utils/is_valid_js_doc'),

    RunBenchmark = require('../benchmark/run_benchmark'),
    RemoveCodeCheckpoints = require('../benchmark/utils/remove_code_checkpoints');

module.exports = [

    new GetChatCompletion(),

    new ExtractModuleFunction(),
    new ExtractModuleVariable(),

    new GetUnitTestTemplate(),

    new RunUnitTest(),
    new UnitTestSuccess(),

    new GenerateCode(),

    new ImplementCode(),
    new ModifyCode(),

    new GenerateUnitTestCases(),
    new WriteUnitTestCode(),
    new SelectBestTestCode(),

    new ImplementUnitTest(),
    new ImplementUnitTestSuite(),

    new GenerateFunctionJsDoc(),
    new GenerateVariableJsDoc(),
    new FillFunctionJsDoc(),

    new IsValidCode(),
    new IsValidJsDoc(),

    new RunBenchmark(),
    new RemoveCodeCheckpoints()

]