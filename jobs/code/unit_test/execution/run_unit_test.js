const { test_framework } = require('../../../../config');
const Job = require('../../../../lib/jobs/Job'),
    test_runner = require('../../../../lib/test_runner'),
    template_builder = require('../../../../lib/template_builder');

class RunUnitTest extends Job {

    name = 'run_unit_test';

    description = 'Run unit test for a JS function';

    inputs = {
        project: {
            type: 'Project',
            description: 'Target project'
        },
        file: {
            type: 'File',
            description: "File object corresponding to the function's module"
        },
        function_name: {
            type: 'string',
            description: 'Name of the function'
        },
        test_template: {
            type: 'object',
            description: 'Template of the unit test',
            auto_fill: true
        },
        test_code: {
            type: 'string',
            description: 'Code of the unit test'
        },
        test_framework: {
            type: 'string',
            description: 'Test framework to use',
            default_value: 'jest'
        }
    };

    outputs = {
        test_results: {
            type: 'object',
            description: 'Results of the unit test'
        }
    };

    execute = async (context) => {
        let project = context.project,
            file = context.file,
            test_code = context.test_code,
            template = context.test_template,
            test_framework = context.test_framework;
        let file_path = file.absolute_path,
            run_code = template_builder.fillTemplateCodePaths(test_code, template.libs, file_path);
        let results = await test_runner.runTestCode(project, run_code, { include_output: true, test_framework });
        return { test_results: results };
    }

}

module.exports = RunUnitTest;