The function you need to test is named "{function_name}".

Below is an extract of the file containing the necessary function:

```javascript
{code}
```

Generate test cases descriptions that will cover all lines and branches of the given function.
Your test descriptions will be used to set up the tests functions (eg it("test description")) and your test cases must cover all lines and branches of the function.
No need to test log function calls but it is important to verify that calls to external functions without return values are done with the correct args.
It is important to focus on providing descriptive test case descriptions rather than technical ones and to indicate both what is being tested and what is the expected result.
It is also important to focus on the behaviour of the tested function instead of the called functions.

You should only respond in JSON format as described below:

RESPONSE FORMAT:
{
    "testCases": [
        "test case description",
        "another test case description"
    ]
}

Please ensure that your response can be parsed using JSON.parse.