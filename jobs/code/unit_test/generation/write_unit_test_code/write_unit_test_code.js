const AiJob = require('../../../../../lib/jobs/AiJob'),
    _ = require('lodash'),
    fse = require('fs-extra'),
    path = require('path'),
    code_utils = require('../../../../../lib/code_utils'),
    prompt = fse.readFileSync(path.join(__dirname, 'write_unit_test_code_prompt.txt'), 'utf8');

const suffix = '    });\n});',
    test_indent = '        ';

class WriteUnitTestCode extends AiJob {

    constructor(props) {
        super();
        _.assign(this, props);
    }

    name = 'write_unit_test_code';

    description = 'Write unit test code for the corresponding test case for a JS function';

    chat_options = {
        prompt_name: 'write_unit_test_code',
        // model: 'gpt-4-turbo',
        model: 'gpt-3.5-turbo',
        max_tokens: 2048
    };

    prompt = prompt;

    inputs = {
        function_name: {
            type: 'string',
            description: 'Name of the function to test'
        },
        code: {
            type: 'string',
            description: 'Code snippet of the function to test'
        },
        test_case: {
            type: 'string',
            description: 'Description of the test case'
        },
        test_template: {
            type: 'object',
            description: 'Template of the unit test'
        },
        additional_instructions: {
            type: 'array',
            description: 'Additional instructions to the AI',
            optional: true
        },
        validate_code: {
            type: 'boolean',
            description: 'Whether to validate the code or not',
            default_value: true
        },
        test_function_count: {
            type: 'integer',
            description: 'Number of test functions to write',
            default_value: 1
        },
        generate_count: {
            type: 'integer',
            description: 'Number of test functions to generate',
            default_value: 1
        },
        chat_options: {
            type: 'object',
            description: 'Chat options for the prompt',
            optional: true
        }
    };

    outputs = {
        test_code: {
            type: 'string',
            description: 'Code of the unit test',
            optional: (result) => {
                return result.test_code_list !== undefined;
            }
        },
        test_code_list: {
            type: 'array',
            description: 'List of code of the unit test',
            optional: (result) => {
                return result.test_code !== undefined;
            }
        }
    };

    execute = async (context, options = {}) => {
        let file = context.file,
            function_name = context.function_name,
            template = context.test_template,
            prompt_context = {
                function_name: file.module_name + '.' + function_name,
                code: context.code,
                test_case: context.test_case,
                additional_instructions: context.additional_instructions,
                test_template: template.filled_content
            },
            chat_options = _.assign({}, this.chat_options, context.chat_options || {}, options.chat_options_override || {}),
            prompt = this.buildPrompt(chat_options.prompt || this.prompt, prompt_context),
            chat_model = context.chat_model;
        if (chat_model) {
            chat_options.model = chat_model;
        }
        let generate_context = {
            prompt: prompt,
            chat_options: chat_options,
            validate_code: context.validate_code,
            test_function_count: context.test_function_count,
            generate_count: context.generate_count,
            options: {
                extract_last_block: 'it'
            }
        }

        let result = await this.jobs.generate_code.setOutput({ code: 'test_code', code_list: 'test_code_list' })(generate_context);
        let code_list = [],
            result_code_list = result.test_code_list || [result.test_code];
        for (let code of result_code_list) {
            let processed_code = processCode(code, template, [function_name + '(']);
            if (processed_code) {
                code_list.push(processed_code);
            }
        }
        return context.generate_count === 1 ? { test_code: code_list[0] } : { test_code_list: code_list };
    }

}

function processCode(code, template, verify_contains = []) {
    let test_function_code = code_utils.getCodeBlockContent(code, 'it', false),
        indent = code_utils.getFirstLineIndent(test_function_code),
        missing_indent = code_utils.computeMissingIndent(indent, test_indent);
    if (missing_indent.length > 0) {
        test_function_code = code_utils.addLinesIndent(test_function_code, missing_indent);
    }
    if (test_function_code) {
        for (let content of verify_contains) {
            if (!test_function_code.includes(content)) {
                return null;
            }
        }
        return template.content_start + '\n' + test_function_code + '\n' + suffix;
    }
    return null;
}

module.exports = WriteUnitTestCode;