const Job = require('../../../../lib/jobs/Job'),
    _ = require('lodash'),
    { pipe } = require('../../../../lib/jobs/jobs');

class ImplementUnitTest extends Job {

    name = 'implement_unit_test';

    description = 'Implement, validate and fix unit test for a function';

    inputs = {
        project: {
            type: 'Project',
            description: 'Target project'
        },
        file: {
            type: 'File',
            description: "File object corresponding to the function's module"
        },
        function_name: {
            type: 'string',
            description: 'Name of the function'
        },
        test_case: {
            type: 'string',
            description: 'Description of the test case'
        },
        additional_instructions: {
            type: 'array',
            description: 'Additional instructions to the AI',
            optional: true
        },
        test_index: {
            type: 'integer',
            description: 'Index of the test case',
            default_value: 0
        },
        test_framework: {
            type: 'string',
            description: 'Test framework to use',
            default_value: 'jest'
        },
    };

    outputs = {
        test_code: {
            type: 'string',
            description: 'Code of the unit test'
        },
        passing_test: {
            type: 'boolean',
            description: 'True if the test is passing'
        }
    };

    execute = async (context) => {
        context.project.matchTestFrameworkRequirements(context.test_framework);
        let strategies = [
            {
                jobs: [
                    this.jobs.extract_module_function.set({
                        include_dependencies: true,
                        max_depth: 0,
                        only_prototypes: true
                    }),
                    this.jobs.remove_code_checkpoints,
                    this.jobs.get_unit_test_template,
                    this.jobs.write_unit_test_code.set({ generate_count: 1, chat_options: context.chat_options || {provider: 'openai', model: 'gpt-3.5-turbo'}  }),
                    // this.jobs.select_best_test_code
                ],
                try_count: 5
            },
            {
                jobs: [
                    this.jobs.extract_module_function.set({
                        include_dependencies: true,
                        max_depth: 0,
                        only_prototypes: true
                    }),
                    this.jobs.remove_code_checkpoints,
                    this.jobs.get_unit_test_template,
                    this.jobs.write_unit_test_code.set({ generate_count: 1, chat_options: {provider: 'openai', model: 'gpt-4o'}  }),
                    // this.jobs.select_best_test_code
                ],
                try_count: 3
            }
        ];

        let last_test_code = "";
        for (let index = 0; index < strategies.length; index++) {
            let strategy = strategies[index];
            for (let i = 0; i < strategy.try_count; i++) {
                try {
                    let test_code = (await pipe(strategy.jobs)(context)).test_code;
                    if (test_code) {
                        last_test_code = test_code;
                        try {
                            let result = await this.verifyAndReturnTestCode(context, test_code, index, i);
                            if(result.passing_test) return result;
                        } catch (err) {

                        }
                    }
                } catch (err) {
                    console.log(err);
                }
            }
        }
        return await this.verifyAndReturnTestCode(context, last_test_code, strategies.length, _.last(strategies).try_count - 1);
    }

    verifyAndReturnTestCode = async (context, test_code, strategy_index, try_count) => {
        let passing_test = false;
        try {
            await pipe([
                this.jobs.extract_module_function,
                this.jobs.get_unit_test_template,
                this.jobs.unit_test_success
            ])({
                ...context,
                test_code
            });
            // context.test_code = test_code;
            // await this.jobs.unit_test_success(context);
            passing_test = true;
        } catch (err) { }
        return { test_code, passing_test, strategy_index, try_count };
    }

}

module.exports = ImplementUnitTest;