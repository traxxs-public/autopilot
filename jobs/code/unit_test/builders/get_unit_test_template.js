const { test_framework } = require('../../../../config');
const Job = require('../../../../lib/jobs/Job'),
    template_builder = require('../../../../lib/template_builder');

class GetUnitTestTemplate extends Job {

    name = 'get_unit_test_template';

    description = 'Get unit test template for a JS function';

    inputs = {
        project: {
            type: 'Project',
            description: 'Project class'
        },
        file: {
            type: 'File',
            description: "File object corresponding to the function's module"
        },
        function_name: {
            type: 'string',
            description: 'Name of the function to test'
        },
        code: {
            type: 'string',
            description: 'Code snippet of the function to test',
            auto_fill: true
        },
        test_index: {
            type: 'integer',
            description: 'Index of the test case',
            default_value: 0
        },
        test_case: {
            type: 'string',
            description: 'Description of the test case',
            optional: true
        },
        setup_code: {
            type: 'string',
            description: 'Setup code for the unit test',
            // auto_fill: true
            optional: true
        },
        test_framework: {
            type: 'string',
            description: 'Test framework to use',
            default_value: "jest"
        }
    };

    outputs = {
        test_template: {
            type: 'object',
            description: 'Template of the unit test'
        }
    };

    execute = async (context) => {
        try{
            let project = context.project,
                file = context.file,
                function_name = context.function_name,
                code = context.code,
                test_index = context.test_index,
                test_case = context.test_case,
                setup_code = context.setup_code,
                test_framework = context.test_framework,
                template = template_builder.initializeModuleFunctionUnitTestTemplate(project, file, code, function_name, {test_framework});
            if(test_case){
                template = template_builder.getFilledModuleFunctionUnitTestTemplateClone(template, test_case, test_index, {setup_code, test_framework});
            }
            return {test_template: template}
        }catch(err){
            this.throw('Error while getting unit test template', err);
        }
    }

}

module.exports = GetUnitTestTemplate;