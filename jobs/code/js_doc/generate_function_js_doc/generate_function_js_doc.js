const AiJob = require('../../../../lib/jobs/AiJob'),
    fse = require('fs-extra'),
    path = require('path'),
    _ = require('lodash'),
    prompt = fse.readFileSync(path.join(__dirname, 'generate_function_js_doc_prompt.txt'), 'utf8');

class GenerateFunctionJsDoc extends AiJob {

    constructor(props) {
        super();
        _.assign(this, props);
    }

    name = 'generate_function_js_doc';

    description = 'Generate JSDoc for a JS function';

    chat_options = {
        prompt_name: 'generate_function_js_doc',
        model: 'gpt-3.5-turbo',
        max_tokens: 2048,
        stop: '*/'
    };

    prompt = prompt;

    inputs = {
        file: {
            type: 'File',
            description: 'File object with the target function'
        },
        function_name: {
            type: 'string',
            description: 'Name of the function'
        },
        code: {
            type: 'string',
            description: 'Code snippet of the function'
        },
        validate_js_doc: {
            type: 'boolean',
            description: 'Whether to validate the JSDoc or not',
            default_value: true
        },
        generate_count: {
            type: 'integer',
            description: 'Number of JSDoc to generate',
            default_value: 1
        },
        chat_options: {
            type: 'object',
            description: 'Chat options for the prompt',
            optional: true
        }
    };

    outputs = {
        js_doc: {
            type: 'string',
            description: 'Generated JSDoc',
            optional: (result) => {
                return result.js_doc_list !== undefined;
            }
        },
        js_doc_list: {
            type: 'array',
            description: 'Generated JSDoc list',
            optional: (result) => {
                return result.js_doc !== undefined;
            }
        }
    };

    execute = async (context, options) => {
        let file = context.file,
            function_name = context.function_name,
            validate_js_doc = context.validate_js_doc,
            generate_count = context.generate_count,
            prompt_context = {
                function_name: file.module_name + '.' + function_name,
                code: context.code
            };
        let job_prompt = this.buildPrompt(this.prompt, prompt_context),
            chat_options = _.assign({}, this.chat_options, context.chat_options || {}, options.chat_options_override || {});

        if (generate_count > 1) {
            chat_options.n = generate_count;
        }
        let results = (await this.jobs.get_chat_completion({
            prompt: job_prompt,
            chat_options: chat_options
        })).completion;

        let js_doc_list = [],
            generated = generate_count === 1 ? [{ result: results }] : results;
        for (let result of generated) {
            let js_doc = processResult(result.result);
            if (validate_js_doc) {
                try {
                    await this.jobs.is_valid_js_doc({ file: context.file, target_type: 'function', target_name: function_name, js_doc: js_doc });
                    js_doc_list.push(js_doc);
                } catch (err) { }
            } else {
                js_doc_list.push(js_doc);
            }
        }
        if (js_doc_list.length === 0) {
            this.throw('No valid JSDoc generated');
        } else {
            return generate_count === 1 ? { js_doc: js_doc_list[0] } : { js_doc_list: js_doc_list };
        }
    }

}

function processResult(result){
    let start_index = result.indexOf('/**'),
        js_doc = result.substring(start_index) + '*/';
    return js_doc;
}

module.exports = GenerateFunctionJsDoc;