## Task: Generate JSDoc for a given JS module variable

## JS variable code extract:
```javascript
{code}
```

## Requirements:
1. Generate JSDoc for the variable: {variable_name}.
2. Provide a clear but concise description of what the variable is used for.
3. Output only the JSDoc.