const Job = require('../../../lib/jobs/Job'),
    _ = require('lodash'),
    code_utils = require('../../../lib/code_utils');

class FillFunctionJsDoc extends Job {

    name = 'fill_function_js_doc';

    description = 'Job template';

    inputs = {
        file: {
            type: 'File',
            description: 'File object with the target function'
        },
        function_name: {
            type: 'string',
            description: 'Name of the function to test'
        },
        overwrite_invalid: {
            type: 'boolean',
            description: 'Whether to overwrite the JSDoc or not if it is invalid',
            default_value: false
        },
        overwrite: {
            type: 'boolean',
            description: 'Whether to overwrite the JSDoc or not',
            default_value: false
        },
        code_extraction_options: {
            type: 'object',
            description: 'Options for code extraction',
            optional: true
        },
        generate_retry: {
            type: 'integer',
            description: 'Number of retries to generate',
            default_value: 3
        }
    };

    outputs = {};

    execute = async (context) => {
        let file = context.file,
            function_name = context.function_name,
            overwrite_invalid = context.overwrite_invalid,
            overwrite = context.overwrite,
            generate_retry = context.generate_retry,
            target = file.function_map[function_name],
            generate = !target.js_doc || overwrite;
        if (!generate && overwrite_invalid) {
            try {
                await this.jobs.is_valid_js_doc({ file, function_name, js_doc: target.js_doc });
            } catch (err) {
                generate = true;
            }
        }
        if (generate) {
            let code_extract_context = _.assign({}, context, context.code_extraction_options || {}, {exclude_target_function_js_doc: true})
            let code = (await this.jobs.extract_module_function(code_extract_context)).code;
            let js_doc_context = {
                function_name,
                file: file,
                code: code,
                include_dependencies: true,
                max_depth: 0,
                only_prototypes: context.extract_only_prototypes,
                exclude_target_function_js_doc: true,
                generate_count: 1,
                select_best: true
            }, js_doc;
            try {
                js_doc = (await this.jobs.generate_function_js_doc.retry(generate_retry)(js_doc_context)).js_doc;
            } catch (err) {
                js_doc_context.include_dependencies = false;
                js_doc = (await this.jobs.generate_function_js_doc.retry(generate_retry)(js_doc_context)).js_doc;
            }
            let target_indent = file.function_map[function_name].indent;
            if (target_indent) {
                let current_indent = code_utils.getFirstLineIndent(js_doc),
                    missing_indent = code_utils.computeMissingIndent(current_indent, target_indent);
                if (missing_indent) {
                    js_doc = code_utils.addLinesIndent(js_doc, missing_indent);
                }
            }
            if (target.js_doc) {
                file.replace('\n' + target.js_doc, "", false);
            }
            file.insertBeforeLine([function_name + ":", function_name + " :"], js_doc);
        } else {
            console.log('JSDoc already exists for ' + function_name);
        }
        return {}
    }

}

module.exports = FillFunctionJsDoc;