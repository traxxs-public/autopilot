const Job = require('../../../lib/jobs/Job'),
    extract = require('../../../parsing/extract');

class ExtractModuleVariable extends Job {

    name = 'extract_module_variable';

    description = 'Extract a module variable code snippet (optionaly with its dependencies)';

    inputs = {
        project: {
            type: 'Project',
            description: 'Target project'
        },
        file: {
            type: 'File',
            description: 'File object'
        },
        variable_name: {
            type: 'string',
            description: 'Name of the variable to extract'
        },
        include_js_doc: {
            type: 'boolean',
            description: 'Include js doc in the extract',
            default_value: true
        },
        include_dependencies: {
            type: 'boolean',
            description: 'Include dependencies in the extract',
            default_value: true
        },
        only_prototypes: {
            type: 'boolean',
            description: 'Only include prototypes in the extract',
            default_value: false
        },
        min_depth: {
            type: 'integer',
            description: 'Minimum depth of dependencies to include',
            default_value: -1
        },
        max_depth: {
            type: 'integer',
            description: 'Maximum depth of dependencies to include',
            default_value: 0
        },
        exclude_target_variable_js_doc: {
            type: 'boolean',
            description: 'Exclude js doc of the target variable',
            default_value: false
        }
    };

    outputs = {
        code: {
            type: 'string',
            description: 'Code snippet of the extracted variable'
        }
    };

    execute = async (context) => {
        let options = {
            include_dependencies: context.include_dependencies,
            min_depth: context.min_depth,
            max_depth: context.max_depth,
            include_js_doc: context.include_js_doc,
            only_prototypes: context.only_prototypes
        }
        if(context.exclude_target_variable_js_doc){
            options.exclude_js_doc_targets = [context.variable_name];
        }
        return {code: extract.extractModuleTarget(context.project, context.file, "variable", context.variable_name, options)};
    }

}

module.exports = ExtractModuleVariable;