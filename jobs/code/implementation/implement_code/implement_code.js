const AiJob = require('../../../../lib/jobs/AiJob'),
    _ = require('lodash'),
    fse = require('fs-extra'),
    path = require('path'),
    prompt = fse.readFileSync(path.join(__dirname, 'implement_code_prompt.txt'), 'utf8');

class ImplementCode extends AiJob {

    constructor(props) {
        super();
        _.assign(this, props);
    }

    name = 'implement_code';

    description = 'Generate code according to given requirements/specifications';

    chat_options = {
        prompt_name: 'implement_code',
        // model: 'gpt-4-turbo',
        model: 'gpt-3.5-turbo',
        max_tokens: 2048
    };

    prompt = prompt;

    inputs = {
        code: {
            type: 'string',
            description: 'Related code snippet'
        },
        specifications: {
            type: 'string',
            description: 'Specifications for the code'
        },
        generate_count: {
            type: 'integer',
            description: 'Number of test functions to generate',
            default_value: 1
        },
        chat_options: {
            type: 'object',
            description: 'Chat options for the prompt',
            optional: true
        }
    };

    outputs = {
        code: {
            type: 'string',
            description: 'Generated code',
            optional: (result) => {
                return result.code_list !== undefined;
            }
        },
        code_list: {
            type: 'array',
            description: 'List of generated codes',
            optional: (result) => {
                return result.code !== undefined;
            }
        }
    };

    execute = async (context, options = {}) => {
        let file = context.file,
            prompt_context = {
                code: context.code,
                specifications: context.specifications,
                target_file: file.name
            },
            chat_options = _.assign({}, this.chat_options, context.chat_options || {}, options.chat_options_override || {}),
            prompt = this.buildPrompt(chat_options.prompt || this.prompt, prompt_context),
            chat_model = context.chat_model;
        if (chat_model) {
            chat_options.model = chat_model;
        }
        let generate_context = {
            prompt: prompt,
            chat_options: chat_options,
            generate_count: context.generate_count,
            validate_code: false
        }

        return await this.jobs.generate_code(generate_context);
    }

}

module.exports = ImplementCode;