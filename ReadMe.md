# autopilot

Autopilot is a framework and set of tools allowing automatic generation of code, unit tests and JSDoc for Javascript.

This project has been developped by engineers at the TRAXxs company to help improve their developpment and quality processes.

# Introduction

Welcome to the Autopilot CLI, a powerful command-line interface designed to streamline and enhance the management of your JavaScript projects. This tool is specifically crafted to automate various tasks, making it easier for developers to maintain code quality, generate documentation, and create comprehensive unit tests. The following document provides an extensive overview of the features and functionalities available in the Autopilot CLI, ensuring you can fully leverage its capabilities for your project needs.

The Autopilot CLI is structured around a modular architecture, with each component focusing on a specific aspect of project management. The core components include the CLI entry point, main project file, configuration management, file status reporting, JSDoc generation, and unit test creation. Each of these components works in tandem to provide a seamless experience, from setting up a new project to maintaining an existing one.

Starting with the CLI entry point (`autopilot-cli.js`), this script is responsible for initializing the CLI and guiding the user through the setup process. It prompts for essential information such as the project path and entry point, ensuring that the necessary configurations are in place before proceeding. Once the setup is complete, the CLI loads the project configuration and initializes the main menu, offering a range of options for further actions.

The main project file (`autopilot.js`) serves as the central hub for job registration and initialization. It loads a predefined list of jobs and registers them with the job management system. This setup allows for easy extension and customization, as new jobs can be added to the job list and seamlessly integrated into the workflow.

Configuration management is handled by `cli/config.js`, which provides functions for loading, saving, and setting configurations. This component ensures that all settings, such as project paths, entry points, and preferred text editors, are consistently applied across the CLI. The configuration file also includes settings for logging, API keys, and LLM (Large Language Model) providers, making it easy to manage external dependencies and integrations.

File status and JSDoc generation are critical features of the Autopilot CLI. The file status component (`cli/file_status.js`) displays the status of variables and functions within a file, highlighting the presence or absence of JSDoc comments and unit tests. This feature helps maintain code quality by ensuring that all necessary documentation and tests are in place. The JSDoc generation components (`cli/js_doc/generate_missing_js_doc.js` and `cli/js_doc/generate_target_js_doc.js`) automate the creation of JSDoc comments, either by identifying and filling in missing comments or by generating documentation for specific targets.

The main menu (`cli/main_menu.js`) and its submenus provide an intuitive interface for navigating the CLI's features. Users can select files, generate JSDoc comments, create unit tests, check file status, and edit configurations through a series of interactive prompts. The file selection (`cli/select_file.js`) and target selection (`cli/select_target.js`) components make it easy to specify the exact file or code element to work on, streamlining the workflow.

Unit test generation is another core feature of the Autopilot CLI. The tool offers options for generating single unit tests (`cli/unit_test/generate_single_unit_test.js`) or complete test suites (`cli/unit_test/generate_unit_test_suite.js`). The unit test menu (`cli/unit_test/unit_test.js`) provides a centralized interface for managing these tests, ensuring that all critical code paths are thoroughly tested.

Utility functions (`cli/utils.js`) support various tasks such as handling paths, opening text editors, and managing unit test output paths. These functions enhance the CLI's usability by automating common tasks and reducing the need for manual intervention.

The configuration file (`config.js`) includes settings for logging, API keys, project paths, and LLM providers. This file ensures that all necessary configurations are easily accessible and modifiable, supporting the seamless integration of external services and tools.

Job definitions are managed within the `jobs/` directory, which includes a job list (`jobs/job_list.js`), job templates, and specific job implementations. These jobs cover a range of tasks, including chat completion, code extraction, code generation, JSDoc generation, and unit test implementation. The job management system (`lib/jobs/`) handles job execution, retry mechanisms, and context handling, ensuring that all jobs are executed efficiently and reliably.

AI and token management components manage interactions with various LLM providers such as OpenAI, DeepSeek, and Groq. The token utilities (`lib/ai/token_utils.js`) track token usage and costs, providing insights into the resource consumption of LLM interactions.

Code utilities (`lib/code_utils.js`) offer functions for extracting, cleaning, and manipulating code blocks, supporting the generation of high-quality documentation and tests. The console menu (`lib/console_menu.js`) provides functions for creating interactive console menus, enhancing the user experience with intuitive and responsive interfaces.

Project and file parsing components manage the loading and parsing of project files (`parsing/project.js`), individual files (`parsing/file.js`), and required modules (`parsing/module_parser.js`). These components ensure that all project dependencies are correctly identified and managed, supporting the seamless integration of new code and documentation.

Finally, the templates directory (`lib/templates/`) provides templates for generating unit tests using Jest or Mocha and Sinon, ensuring consistency and best practices in test creation.

The Autopilot CLI is a comprehensive tool designed to automate and enhance JavaScript project management. Its modular architecture, extensive feature set, and intuitive interface make it an invaluable asset for developers seeking to maintain high code quality and streamline their workflows. Whether you are setting up a new project or maintaining an existing one, the Autopilot CLI offers the tools and functionalities you need to succeed.

# Installation

To install the AutoPilot project, follow the steps outlined below. This guide will ensure that you have all the necessary dependencies and configurations set up correctly for a smooth installation process.

### Prerequisites

Before you begin, make sure you have the following software installed on your machine:

1. **Node.js**: The AutoPilot project requires Node.js. You can download and install it from [Node.js official website](https://nodejs.org/). We recommend using the latest LTS version.
2. **npm**: npm (Node Package Manager) is usually installed alongside Node.js. You can check if npm is installed by running `npm -v` in your terminal.
3. **Git**: Git is required for cloning the repository. You can download it from [Git official website](https://git-scm.com/).

### Cloning the Repository

First, clone the AutoPilot repository to your local machine. Open your terminal and run the following command:

```bash
git clone https://github.com/alextis59/autopilot
```

Navigate to the project directory:

```bash
cd autopilot
```

### Installing Dependencies

Once you have navigated to the project directory, install the required dependencies. The AutoPilot project uses npm to manage its dependencies. Run the following command to install them:

```bash
npm install
```

This command will read the `package.json` file and install all the necessary packages listed under `dependencies` and `devDependencies`.

### Configuration

The AutoPilot project requires some configurations to be set up before it can be run. These configurations are stored in a `.config` file.

### Setting Up the Environment

The AutoPilot project relies on certain environment variables for its operation. Create a `.env` file in the root directory of the project and add the required environment variables. Here is an example of what the `.env` file might look like:

```plaintext
OPENAI_API_KEY=your-openai-api-key
DEEPSEEK_API_KEY=your-deepseek-api-key
GROQ_API_KEY=your-groq-api-key
LOCAL_LLM_URL=http://localhost:8000
LOCAL_LLM_ACCESS_TOKEN=your-local-llm-access-token
```

Make sure to replace the placeholder values with your actual API keys and URLs.

### Running the Project

Once you have installed the dependencies and configured the environment, you can run the AutoPilot project. Use the following command to start the application:

```bash
npm start
```

This command will start the CLI tool, and you should see a welcome message indicating that the AutoPilot project is running. Follow the on-screen prompts to interact with the CLI.

### Additional Setup for Unit Tests

If you plan to generate and run unit tests, ensure that the `unit_test_output_path` and `test_run_file_path` in your configuration are correctly set. The default paths are:

```json
{
  "unit_test_output_path": "test/unit",
  "test_run_file_path": "test/autopilot/tmp/tmp_test.js"
}
```

You can modify these paths in the `.config` file as per your project structure.

# Usage

To effectively utilize the Autopilot CLI for managing your JavaScript projects, follow the steps outlined below. This guide will walk you through the initial setup, configuration, and usage of the various features provided by the tool.

First, ensure that you have Node.js installed on your system as the CLI relies on it for execution. You can download and install Node.js from [nodejs.org](https://nodejs.org/).

### Initial Setup

Once the dependencies are installed, you can launch the CLI tool. The first time you run the CLI, it will prompt you to configure the project path and the entry point:
```bash
./autopilot-cli.js
```
You will be asked to provide the path to your project and the entry point file (typically `index.js`). This information is stored in the configuration file located at `.autopilot/config.json`.

### Main Menu

Upon launching the CLI, you will be greeted with the main menu. This menu offers several options to manage and enhance your project:

1. **Select File**: Allows you to choose a file from your project. This is useful for performing operations on specific files.
2. **Generate JSDoc**: Provides options to either generate JSDoc comments for missing documentation or for a specific target (function or variable).
3. **Generate Unit Tests**: Offers the ability to create unit tests for individual functions or generate a complete test suite for the selected file.
4. **Check File Status**: Displays the status of variables and functions in the selected file, indicating whether they have JSDoc comments and unit tests.
5. **Edit Configuration**: Lets you modify the CLI's configuration settings.

### File Selection

To select a file within your project, choose the "Select File" option from the main menu. You will be presented with a list of files in your project directory. Navigate through the list and select the desired file. The selected file will then be set as the target for subsequent operations.

### JSDoc Generation

The CLI provides robust functionality for managing JSDoc comments:

- **Generate Missing JSDoc**: Automatically identifies functions and variables lacking JSDoc comments and generates them. This feature ensures your codebase is well-documented and adheres to best practices.
- **Generate Target JSDoc**: Allows you to specify a particular function or variable for which JSDoc comments should be generated. This is useful for incrementally improving documentation.

### Unit Test Generation

Creating unit tests is crucial for maintaining the reliability of your code. The CLI offers two main options for generating unit tests:

- **Generate Single Unit Test**: Select a specific function or variable, and the CLI will generate a unit test for it. This is ideal for targeted testing.
- **Generate Unit Test Suite**: Automatically generates a comprehensive suite of unit tests for all functions and variables in the selected file. This ensures that your entire file is thoroughly tested.

### File Status

To get an overview of the documentation and testing status of a file, use the "Check File Status" option. This feature lists all variables and functions in the selected file, indicating whether they have associated JSDoc comments and unit tests. It provides a quick way to identify areas that need attention.

### Configuration Management

The CLI's behavior can be customized through its configuration file. Key settings include:

- **Project Path**: The root directory of your project.
- **Entry Point**: The main file of your project (e.g., `index.js`).
- **Unit Test Output Path**: The directory where generated unit tests will be saved.
- **Text Editor**: The default text editor to open files (e.g., `gedit`).
- **LLM Providers**: Configuration for different language model providers like OpenAI, DeepSeek, and Groq.

To edit these settings, select the "Edit Configuration" option from the main menu. Changes will be saved to the configuration file, and the CLI will use these settings in future sessions.

# Features

## Configuration Management

### Configuration Handling

The configuration handling for the Autopilot CLI tool is an essential component that ensures seamless operation and customization of the tool according to the user’s requirements. This section delves into the specifics of how configurations are managed, stored, and utilized within the CLI, providing a robust framework for adjusting various settings that influence the behavior and capabilities of the tool.

At the core of the configuration management system is the `cli/config.js` file, which is responsible for loading, saving, and modifying the configuration settings. This file ensures that all configurations are consistently applied across different components of the CLI, maintaining a coherent and predictable environment for the user.

The configuration settings are stored in a JSON file, typically located at `.autopilot/config.json` within the project directory. This file includes key parameters such as the project path, entry point, file paths for unit test outputs, and the preferred text editor, among others. Additionally, it contains settings for various models used in generating JSDoc comments and unit tests, which are crucial for the tool’s AI-driven functionalities.

When the CLI is first executed, it checks for the existence of the configuration file. If the file is not found, the CLI initiates a setup process where the user is prompted to provide essential information such as the project path and entry point. This information is then saved into the configuration file for future reference. If the configuration file already exists, it is loaded into memory, and its settings are applied to the CLI’s operations.

The configuration management system also provides functions for modifying and saving configurations. The `set` function allows for updating specific configuration settings, while the `saveConfig` function ensures that any changes are written back to the configuration file. This dynamic handling of configurations enables users to adjust settings on-the-fly without needing to manually edit the JSON file.

A notable feature of the configuration handling system is its support for model-specific settings. For instance, users can specify different models for generating JSDoc comments and unit tests, allowing for tailored AI interactions based on the complexity and requirements of the project. These settings are stored under respective keys within the configuration file, such as `js_doc.model` and `write_unit_test.model`.

The configuration file also includes settings for logging, which can be crucial for debugging and monitoring the tool’s performance. Users can enable or disable logging, set the log level, and specify the path where log files should be stored. This flexibility ensures that users can keep track of the tool’s activities and diagnose any issues that may arise.

Moreover, the configuration management system integrates seamlessly with the CLI’s menu-driven interface. Users can access and modify configuration settings through the main menu, making it easy to adjust settings without delving into the underlying JSON file. This user-friendly approach enhances the overall usability of the tool, especially for those who may not be comfortable with manual configuration file edits.

In summary, the configuration handling system of the Autopilot CLI is a comprehensive and user-centric component that ensures the tool operates effectively and can be customized to meet the specific needs of different projects. By providing a robust framework for managing configurations, the system enhances the flexibility, usability, and overall functionality of the CLI, making it an indispensable part of the Autopilot project management toolset.

## File Status and JSDoc Generation

### File Status

The "File Status" functionality in the Autopilot CLI is a powerful feature designed to provide developers with a comprehensive overview of the current state of their project's files. This feature is particularly useful for maintaining code quality and ensuring that documentation and testing practices are consistently applied throughout the codebase.

When invoked, the "File Status" command scans the specified file within the project and generates a detailed report on the status of each variable and function. The primary focus of this report is to indicate whether JSDoc comments and unit tests are present for each code element, which are critical components for code maintainability and reliability.

The status report is presented in a clear and organized manner, making it easy for developers to quickly identify areas that require attention. Each variable and function is listed along with color-coded indicators that signify the presence or absence of JSDoc comments and unit tests. Specifically, the indicators use green to denote that the required documentation or tests are present, and red to highlight any deficiencies.

For variables, the report checks if JSDoc comments are provided, which are essential for understanding the purpose and usage of each variable. These comments help other developers (and the future self) to quickly grasp the context and functionality of the code without having to delve into the implementation details.

For functions, the report is more comprehensive, as it checks for both JSDoc comments and unit tests. JSDoc comments for functions typically include descriptions of the function’s purpose, details of its parameters, the return value, and any exceptions that might be thrown. This level of documentation is invaluable for ensuring that the code is self-explanatory and that the function's interface is well-defined.

Unit tests, on the other hand, are crucial for verifying that the function behaves as expected under various conditions. The presence of unit tests indicates that the function has been tested for correctness, which helps in catching bugs early in the development process and provides a safety net for future code changes.

The "File Status" feature also integrates seamlessly with the rest of the Autopilot CLI toolset. If any deficiencies are found, developers can use other commands within the CLI to address them. For example, missing JSDoc comments can be generated using the "Generate Missing JSDoc" command, and unit tests can be created using the "Generate Single Unit Test" or "Generate Unit Test Suite" commands. This integration ensures that developers can quickly and efficiently bring their files up to the desired standards without leaving the CLI environment.

In summary, the "File Status" functionality is an essential tool for maintaining high standards of code quality in JavaScript projects. By providing a clear and actionable report on the documentation and testing status of each file, it enables developers to ensure that their code is well-documented, thoroughly tested, and easy to understand. This not only improves the immediate quality of the code but also contributes to long-term maintainability and ease of collaboration.

### Generate Missing JSDoc

The "Generate Missing JSDoc" functionality in the Autopilot CLI is a powerful feature designed to ensure comprehensive documentation of your JavaScript codebase. This feature is particularly useful for maintaining code quality and readability by automatically generating JSDoc comments for functions and variables that lack them. Here's a detailed overview of how this functionality works and its benefits:

When invoked, the "Generate Missing JSDoc" command scans the selected file for any functions or variables that are missing JSDoc comments. It does this by leveraging the `getMissingJSDocTargets` function, which inspects the file's `function_map` and `variable_map` to identify undocumented code elements. This initial scan provides a list of targets that require JSDoc generation.

Once the targets are identified, the tool proceeds to generate the necessary JSDoc comments. This process involves the `generateJSDoc` function, which is responsible for creating the appropriate documentation for each target. Depending on whether the target is a function or a variable, the function sets the context accordingly and extracts the relevant code snippet. For functions, it sets the `function_name` in the context and uses the `extract_module_function` job to retrieve the code, while for variables, it sets the `variable_name` and uses the `extract_module_variable` job.

The extracted code is then passed to the respective JSDoc generation jobs—`generate_function_js_doc` for functions and `generate_variable_js_doc` for variables. These jobs interact with language models to produce the JSDoc comments, ensuring they are accurate and comprehensive. The generated JSDoc is then applied to the target within the file. If a JSDoc comment already exists, it is replaced with the newly generated one. This ensures that the documentation is always up-to-date and consistent with the latest code changes.

The tool also provides feedback throughout the process. It lists the missing JSDoc targets and informs the user when the generation and application of JSDoc comments are complete. If any errors occur during the generation process, such as issues with code extraction or interaction with the language model, these are logged, and the user is notified.

This automated approach to JSDoc generation offers several advantages:

1. **Consistency**: By automating the generation of JSDoc comments, the tool ensures that all functions and variables are documented in a consistent manner. This reduces the likelihood of discrepancies and errors in the documentation.

2. **Time-Saving**: Manually writing JSDoc comments can be time-consuming, especially for large codebases. The automation provided by Autopilot CLI significantly reduces the time required to document code, allowing developers to focus on more critical tasks.

3. **Improved Code Quality**: Comprehensive documentation is crucial for code maintainability and readability. By ensuring that all functions and variables are documented, the tool helps improve the overall quality of the codebase.

4. **Ease of Use**: The feature is integrated seamlessly into the Autopilot CLI, making it easy to use. Developers can generate missing JSDoc comments with a few simple commands, without needing to switch contexts or use additional tools.

In summary, the "Generate Missing JSDoc" feature is an essential component of the Autopilot CLI that enhances the documentation process for JavaScript projects. It automates the generation of JSDoc comments, ensuring consistency, saving time, and improving code quality. This feature is particularly beneficial for maintaining large codebases, where manual documentation would be impractical and error-prone.

### Generate Target JSDoc

The "Generate Target JSDoc" functionality is a powerful feature within the Autopilot CLI that enables users to create JSDoc comments for specific functions or variables in their JavaScript projects. This feature is particularly useful for developers who aim to maintain high-quality documentation standards and ensure that their codebase is well-documented and easy to understand for other developers.

When using the "Generate Target JSDoc" option, the tool prompts the user to select a specific target within a file, which can either be a function or a variable. Upon selection, the tool extracts the relevant code for the chosen target and generates a comprehensive JSDoc comment. This process involves several steps to ensure the generated documentation is accurate and useful.

First, the context for the target is established. If the target is a function, the context includes the function's name and its associated code. Similarly, if the target is a variable, the context includes the variable's name and its code snippet. This context is crucial as it provides the necessary information for the JSDoc generation process.

Next, the tool invokes specific jobs designed to generate JSDoc comments. For functions, the job responsible for generating function-specific JSDoc comments is executed. This job analyzes the function's code, identifies its parameters, return type, and any other relevant details, and constructs a detailed JSDoc comment. For variables, a similar job is executed, which focuses on generating appropriate documentation for the variable, including its type and description.

The generated JSDoc comment is then presented to the user, who has several options for handling it. The user can choose to apply the JSDoc comment directly to the code, copy it to the clipboard for manual insertion, or regenerate the JSDoc if the initial generation did not meet their expectations. Applying the JSDoc comment involves inserting it at the appropriate location in the code, ensuring that the documentation is immediately available and visible to anyone reviewing the code.

This feature is designed to be highly interactive and user-friendly. It provides clear prompts and feedback throughout the process, making it easy for users to generate and apply JSDoc comments without needing to manually write them from scratch. This not only saves time but also ensures consistency and accuracy in the documentation.

Additionally, the tool includes mechanisms to handle errors and retries. If the JSDoc generation process encounters an error, it can retry the operation up to a specified number of times. This ensures that transient issues do not prevent the successful generation of JSDoc comments.

Overall, the "Generate Target JSDoc" feature is a valuable addition to the Autopilot CLI, offering an efficient and effective way to document JavaScript code. By automating the generation of JSDoc comments, it helps developers maintain high documentation standards with minimal effort, ultimately leading to a more maintainable and understandable codebase.

## Main Menu and Submenus

### Main Menu

The main menu serves as the central hub of the Autopilot CLI, providing users with a comprehensive interface to access and manage the tool's various functionalities. Upon launching the CLI, users are greeted with a welcoming message and a prompt displaying the main menu options. This menu is designed to streamline navigation and enhance user experience by offering a structured and intuitive layout.

The main menu dynamically adjusts its options based on the current context, particularly whether a file has been selected. If no file is selected, the menu primarily focuses on project-wide actions. Conversely, if a file is selected, additional file-specific actions become available. This context-sensitive approach ensures that users are presented with relevant options at all times.

Key options available in the main menu include:

1. **Select File**: This option allows users to browse and select a specific file from the project. Upon selection, the chosen file is loaded into the CLI context, enabling file-specific operations. The file selection process involves presenting a list of available files, from which the user can choose.

2. **JSDoc Generation**: When a file is selected, this option becomes available, providing tools to generate JSDoc comments. Users can choose to generate JSDoc for the entire file or target specific functions or variables. This feature significantly aids in maintaining comprehensive documentation throughout the codebase.

3. **Unit Test Generation**: This option enables users to create unit tests for the selected file. It offers the flexibility to generate a single unit test for a specific function or variable, or a complete suite of tests for the entire file. This functionality is crucial for ensuring code reliability and robustness.

4. **File Status**: This feature provides an overview of the selected file, highlighting the presence of JSDoc comments and unit tests for each function and variable. It serves as a quick reference to identify areas that may require additional documentation or testing.

5. **Edit Configuration**: Users can access and modify the CLI configuration settings through this option. It opens the configuration file in the user's preferred text editor, allowing for easy adjustments to settings such as project paths, entry points, and output directories.

6. **Return**: This option allows users to exit the main menu and return to the previous context or the CLI prompt. It provides a straightforward way to navigate back without making any changes.

The main menu's design emphasizes ease of use and efficiency. By presenting a clear and concise set of options, it ensures that users can quickly access the tools they need to manage their JavaScript projects effectively. The dynamic nature of the menu, adapting to the current context, further enhances its utility, making the Autopilot CLI a powerful and user-friendly tool for developers.

### Select File

The "Select File" functionality within the Autopilot CLI provides an intuitive and interactive way for users to navigate and select files within their JavaScript project. This feature is crucial for targeting specific files when performing various tasks such as generating JSDoc comments, creating unit tests, or checking the status of files.

Upon invoking the "Select File" option from the main menu, the CLI presents the user with a list of all available files in the project. This list is dynamically generated based on the project's file structure, ensuring that all relevant files are displayed. Each file in the list is identified by its project path, making it easy for users to locate the file they need to work on.

The selection process is straightforward. Users can scroll through the list and choose a file by selecting its corresponding action. The CLI then updates the context to reflect the selected file, which becomes the active file for subsequent operations. This seamless transition ensures that users can quickly move from file selection to other tasks without unnecessary steps.

In addition to listing the files, the "Select File" feature also includes a "Return" option, allowing users to exit the file selection menu and return to the main menu. This flexibility ensures that users are not locked into the file selection process and can easily navigate back to the main menu to access other functionalities.

The "Select File" feature is designed to handle various scenarios, including projects with a large number of files or complex directory structures. It leverages efficient file handling mechanisms to ensure that even extensive file lists are displayed promptly and accurately. The underlying implementation uses the CLI context to keep track of the selected file, which is crucial for maintaining consistency across different operations.

Moreover, the "Select File" functionality integrates seamlessly with other features of the Autopilot CLI. For instance, once a file is selected, users can immediately proceed to generate JSDoc comments, create unit tests, or check the file's status without needing to reselect the file. This integration enhances the overall user experience by reducing redundant steps and streamlining the workflow.

In summary, the "Select File" feature is a vital component of the Autopilot CLI, providing users with a powerful and user-friendly interface for navigating and selecting files within their project. Its integration with other features ensures a cohesive and efficient workflow, making it an indispensable tool for JavaScript project management.

### Select Target

The "Select Target" functionality within the Autopilot CLI is a critical feature designed to enhance the precision and efficiency of JavaScript project management. This feature allows users to pinpoint specific variables or functions within a selected file, facilitating targeted operations such as JSDoc generation, unit test creation, and code analysis.

When invoked, the "Select Target" menu presents users with a list of available targets within the currently selected file. These targets are categorized into variables and functions, each represented clearly to aid in quick identification. The user interface is intuitive, displaying the names of variables and functions, along with their respective types, making it straightforward for users to navigate and select the desired target.

The process begins with the system fetching the variable and function maps from the selected file. The variable map contains all the variables declared within the file, while the function map lists all the functions. Each entry in these maps is then presented as an actionable item in the menu. Users can scroll through the list and choose a specific variable or function by selecting the corresponding menu item.

The menu is designed to be interactive and user-friendly. Each target is displayed with a clear title indicating its name and type (e.g., "variable_name (var)" or "function_name (fn)"). This categorization helps users quickly differentiate between variables and functions, ensuring they can make precise selections without confusion. Additionally, the menu includes a "Return" option, allowing users to navigate back to the previous menu if they need to change the selected file or perform other actions.

Upon selection of a target, the system updates the CLI context to include the chosen variable or function. This context update is crucial as it informs subsequent operations about the specific target that the user intends to work with. For instance, if the user proceeds to generate a JSDoc or a unit test, the system will apply these operations specifically to the selected target, ensuring that the output is relevant and accurate.

The "Select Target" feature is particularly beneficial in large codebases where files may contain numerous variables and functions. By allowing users to focus on a single target, the Autopilot CLI streamlines the workflow, reducing the time and effort required to perform documentation and testing tasks. This targeted approach not only improves efficiency but also enhances the accuracy of the generated documentation and tests.

Moreover, the feature supports advanced options that can be customized to suit specific needs. For instance, users can configure color coding for targets based on their characteristics or importance, further aiding in quick identification. These customization options make the "Select Target" feature adaptable to various project requirements and user preferences.

In summary, the "Select Target" functionality within the Autopilot CLI is an indispensable tool for developers working on JavaScript projects. It provides a streamlined, intuitive interface for selecting specific variables and functions within a file, thereby facilitating precise and efficient project management tasks. Whether generating JSDoc comments, creating unit tests, or performing code analysis, this feature ensures that users can focus on their desired targets with ease and accuracy.

## Unit Test Generation

### Generate Single Unit Test

The "Generate Single Unit Test" functionality within the Autopilot CLI is designed to streamline and automate the creation of unit tests for individual functions or variables in a JavaScript project. This feature is particularly useful for developers who aim to maintain high code quality and ensure thorough testing of their codebase without manually writing each test case.

To generate a single unit test, the user interacts with the Autopilot CLI's intuitive interface, which guides them through a series of prompts to specify the target for the unit test. The process begins by selecting the file containing the function or variable to be tested. Once the file is chosen, the user is prompted to select the specific function or variable within that file. This selection process is facilitated by the CLI's ability to list all available functions and variables, making it easy for the user to pinpoint the exact target.

After selecting the target, the user is asked to provide a description for the test case. This description helps in defining the scope and purpose of the test, ensuring that it covers the intended functionality. The CLI then leverages its integrated job management system to generate the unit test. This system includes a series of predefined job templates and strategies designed to create comprehensive and effective test cases.

The unit test generation process involves several key steps. First, the CLI uses the "Generate Unit Test Cases" job to create a list of potential test cases for the selected function or variable. This job utilizes advanced AI models to analyze the code and generate test cases that cover various code paths and edge cases. The generated test cases are then evaluated to ensure they meet the required standards.

Next, the "Write Unit Test Code" job takes the selected test case and generates the corresponding test code. This job uses a combination of predefined templates and AI-driven code generation techniques to produce high-quality test code. The generated test code is formatted according to industry standards and includes all necessary setup and validation logic.

Once the test code is generated, it is displayed to the user for review. The user has the option to save the test code to a file, copy it to the clipboard, or edit it further using their preferred text editor. The CLI provides convenient options for managing the generated test code, including overwriting existing test files or creating new ones.

The generated unit test is designed to be compatible with popular testing frameworks like Jest or Mocha and Sinon. This ensures that the test code can be seamlessly integrated into the project's existing test suite. The CLI also provides utilities for running the generated tests and verifying their correctness, making it easy for developers to validate their code changes.

Overall, the "Generate Single Unit Test" feature of the Autopilot CLI significantly reduces the effort required to create and maintain unit tests. By automating the test generation process and leveraging advanced AI capabilities, it enables developers to focus on writing high-quality code while ensuring comprehensive test coverage. This feature is an essential tool for any JavaScript project aiming to achieve robust and reliable software.

### Generate Unit Test Suite

The Autopilot CLI offers a comprehensive feature for generating unit test suites, which is a crucial component for ensuring the reliability and robustness of JavaScript codebases. This functionality is designed to automate the creation of a complete set of unit tests for a given target, whether it be a function or a variable within a project file. The process is streamlined to save developers significant time and effort, while also enhancing code quality by providing thorough test coverage.

To initiate the generation of a unit test suite, the user navigates through the main menu of the CLI and selects the option to generate a complete unit test suite. This action triggers a series of steps where the CLI interacts with the project's context to gather necessary information about the target for which the tests are to be generated.

The core of this feature is built around the `generate_unit_test_suite.js` module, which orchestrates the entire process. Initially, the system collects the relevant context, including the project details, the specific file, and the target function or variable. It then leverages the `getTestCases` function to dynamically generate a comprehensive list of test cases. These test cases are designed to cover all possible execution paths, edge cases, and potential failure points within the target code.

Once the test cases are generated, the CLI proceeds to implement these cases into executable test code. This involves invoking the `implement_unit_test_suite` job, which meticulously constructs the test code using predefined templates and the generated test cases. The result is a cohesive suite of unit tests that can be immediately executed to validate the functionality of the target code.

The generated test suite is then presented to the user, who can review the test code directly within the CLI interface. The user is given the option to specify the output path for the test files, ensuring that the tests are stored in the desired location within the project directory. If the specified file already exists, the CLI offers options to overwrite the existing file, copy the test code to the clipboard, or cancel the operation, providing flexibility in handling existing test files.

Throughout this process, the CLI ensures that the generated unit tests adhere to best practices and coding standards. The tests are structured using Jest or Mocha and Sinon, popular frameworks for unit testing in JavaScript, ensuring compatibility with existing testing infrastructure. Additionally, the CLI integrates with the project's configuration settings to apply any custom configurations or preferences specified by the user.

In summary, the unit test suite generation feature of the Autopilot CLI is a powerful tool that automates the creation of comprehensive and high-quality unit tests. By leveraging advanced context handling and dynamic test case generation, it significantly reduces the manual effort required to write tests, while also enhancing the reliability and maintainability of the codebase. This feature is an indispensable asset for developers aiming to maintain rigorous testing standards and ensure the robustness of their JavaScript projects.

### Unit Test Menu

The Unit Test Menu in the Autopilot CLI is a powerful feature designed to streamline the process of creating and managing unit tests for JavaScript projects. This menu provides users with a variety of options to generate unit tests efficiently, ensuring that code is well-tested and reliable. 

Upon accessing the Unit Test Menu, users are presented with multiple actions that cater to different testing needs. The primary functionalities include generating a single unit test for a specific target within a file and creating a comprehensive suite of unit tests for a selected target. This flexibility allows developers to focus on individual functions or variables as well as perform extensive testing across entire modules.

The process of generating a single unit test begins with the selection of a target, which can be either a function or a variable within the chosen file. The CLI then prompts the user to provide a description for the test case, ensuring that the generated test is meaningful and contextually relevant. Utilizing the underlying job execution framework, the CLI generates the unit test code, leveraging AI models specified in the configuration to ensure high-quality output. The generated test code is then displayed to the user, who can choose to save it to a file, copy it to the clipboard, or overwrite an existing test file.

For more extensive testing needs, the Unit Test Menu offers the option to generate a complete unit test suite. This functionality automates the creation of multiple test cases, covering various aspects of the selected function or variable. The suite generation process is similar to that of a single test but involves creating a series of test cases, each tailored to different scenarios and edge cases. This comprehensive approach ensures that the code is thoroughly tested, reducing the likelihood of bugs and enhancing overall code quality.

In addition to generating unit tests, the Unit Test Menu provides options for managing the generated tests. Users can validate the test cases to ensure they align with the expected behavior of the code. If necessary, the menu also allows for the regeneration of test cases, providing an opportunity to refine and improve the tests based on new insights or changes in the codebase.

The integration of utility functions within the Unit Test Menu further enhances its usability. For instance, the CLI can automatically determine the appropriate output path for the unit tests, ensuring that they are organized and easily accessible. Additionally, the menu supports interactive prompts, guiding users through the process and making it intuitive even for those who may not be familiar with unit testing.

Overall, the Unit Test Menu in the Autopilot CLI is an indispensable tool for developers aiming to maintain high standards of code quality. By automating the generation of unit tests and providing a user-friendly interface, it significantly reduces the effort required to implement robust testing practices. Whether generating a single test or a full suite, the Unit Test Menu ensures that JavaScript projects are well-tested and reliable, ultimately leading to more stable and maintainable codebases.

## Configuration File

### Configuration Settings

The configuration settings of the Autopilot CLI are pivotal for tailoring the tool to fit specific project needs and user preferences. These settings are managed within the `config.js` file, which acts as the central repository for all configurable parameters. This file encompasses a wide array of settings, including paths, logging preferences, API keys, and model specifications for various functionalities such as JSDoc generation and unit test creation.

The configuration settings are structured hierarchically to ensure clarity and ease of access. At the top level, the configuration includes essential project details like the `project_path` and `entry_point`. These fields are critical as they define the root directory of the project and the main file that the CLI will use to initiate the project setup and subsequent operations.

Another significant configuration aspect is the `unit_test_output_path`, which specifies the directory where generated unit tests will be stored. This setting ensures that all unit tests are organized in a dedicated location, making it easier to manage and review them. Additionally, the `text_editor` configuration allows users to specify their preferred text editor for opening and editing files directly from the CLI, enhancing the user experience by integrating seamlessly with their existing development environment.

The `js_doc` and `write_unit_test` configurations are particularly noteworthy as they define the models used for generating JSDoc comments and unit tests, respectively. For instance, the `js_doc` configuration may specify a model like `llama3-70b-8192` for generating comprehensive documentation comments, while the `write_unit_test` configuration might use a different model optimized for creating unit tests. These settings enable the CLI to leverage different AI models tailored to specific tasks, ensuring high-quality output.

The `generate_unit_test_cases` configuration further refines the unit test generation process by specifying a model for creating detailed test cases. This model can be different from the one used for writing unit tests, allowing for greater flexibility and optimization based on the complexity and requirements of the test cases.

Logging is another crucial aspect of the configuration settings. The `log` configuration includes parameters such as `enabled`, `level`, and `log_file`, which control the logging behavior of the CLI. Users can enable or disable logging, set the verbosity level, and specify whether logs should be written to a file. The `path` parameter within the `log` configuration defines the directory where log files will be stored, ensuring that all log data is systematically archived for future reference.

API keys for various LLM providers are also managed within the configuration settings. The `openai`, `deepseek`, and `groq` configurations each contain an `api_key` field where users can input their respective API keys. These keys are essential for authenticating and interacting with the different LLM providers, enabling the CLI to perform tasks such as chat completions and code generation.

The `local_llm` configuration is designed for users who prefer to use a local language model. This setting includes parameters such as `url` and `access_token`, which define the endpoint and authentication token for the local model. This feature is particularly useful for users who require a more private or customized AI solution.

Additionally, the configuration settings include options for managing dependencies and file parsing. The `dependencies_filter` and `disable_parsing_file_list` configurations allow users to specify which files and dependencies should be included or excluded during the parsing process. This level of control ensures that the CLI operates efficiently by focusing only on the relevant parts of the project.

In summary, the configuration settings of the Autopilot CLI provide a comprehensive framework for customizing the tool to meet specific project requirements and user preferences. By defining paths, logging preferences, API keys, and model specifications, these settings ensure that the CLI operates seamlessly and efficiently, delivering high-quality output tailored to the user's needs.

## AI and Token Management

### LLM Providers and models

The integration of Language Model (LLM) providers within the Autopilot CLI tool is a pivotal feature that enhances its capabilities in code generation, validation, and other AI-driven tasks. This section delves into the various LLM providers supported by the tool, describing their configurations, functionalities, and how they are utilized to improve the overall user experience.

Autopilot CLI supports multiple LLM providers, each offering unique models and capabilities tailored to different aspects of project management and code handling. The primary providers integrated into the tool include OpenAI, DeepSeek, and Groq. Each of these providers offers distinct models that can be configured and leveraged according to the specific needs of the project.

### OpenAI Integration

OpenAI is one of the most prominent LLM providers integrated into the Autopilot CLI. It supports various models, including `gpt-3.5-turbo` and `gpt-4-turbo`, known for their advanced natural language processing capabilities. The OpenAI integration is configured through the `openai` section in the configuration file, where users can set their API key, request timeout, and other parameters.

The OpenAI models are particularly effective for tasks that require high-quality text generation, such as generating JSDoc comments, creating unit tests, and providing code completions. The models support multiple response generation, allowing the tool to offer several suggestions or completions for a given task. This feature enhances the flexibility and robustness of the tool, enabling users to choose the most suitable output for their needs.

### DeepSeek Integration

DeepSeek is another LLM provider supported by the Autopilot CLI, known for its specialized models like `deepseek-coder`. This provider is configured through the `deepseek` section in the configuration file, where users can set the API key and request timeout. DeepSeek models are optimized for code-related tasks, making them ideal for generating code snippets, performing code analysis, and other development-centric activities.

The DeepSeek integration allows the Autopilot CLI to leverage its models for tasks that require deep understanding and manipulation of code. This includes generating missing JSDoc comments, extracting code blocks, and validating code against predefined standards. The integration ensures that the tool can provide precise and contextually relevant outputs, enhancing the efficiency and accuracy of the development process.

### Groq Integration

Groq is the third LLM provider integrated into the Autopilot CLI, offering models such as `llama3-8b-8192`. Configured through the `groq` section in the configuration file, users can set the API key and request timeout for this provider. Groq models are designed to handle large-scale text generation and analysis tasks, making them suitable for comprehensive code generation and validation activities.

The Groq integration enhances the tool's ability to manage extensive and complex codebases. It supports tasks like generating complete unit test suites, performing detailed code reviews, and providing advanced code completions. The models' capacity to handle large token counts ensures that even the most intricate and voluminous code segments can be processed efficiently and accurately.

### Token Management

Effective token management is crucial for optimizing the use of LLM providers. The Autopilot CLI includes utilities for managing token usage and tracking costs associated with LLM interactions. The `token_utils.js` module provides functions for encoding strings, calculating token counts, and managing session tokens. This ensures that the tool can efficiently utilize the available token limits, preventing overuse and optimizing performance.

Each LLM provider has specific token limits and cost structures, which are defined in the configuration file. The tool tracks token usage per session and provides mechanisms to handle rate limits and retry mechanisms. This ensures that the tool can maintain a steady and efficient interaction with the LLM providers, even under high-demand scenarios.

### Conclusion

The integration of multiple LLM providers within the Autopilot CLI tool significantly enhances its capabilities in managing JavaScript projects. By leveraging the advanced models offered by OpenAI, DeepSeek, and Groq, the tool can perform a wide range of AI-driven tasks with high precision and efficiency. The comprehensive token management system ensures optimal usage of these models, making the Autopilot CLI a powerful and reliable tool for developers.

## Console Menu

### Interactive Console

The interactive console within the Autopilot CLI offers a dynamic and user-friendly interface for managing various aspects of JavaScript projects. This feature significantly enhances the usability of the tool by providing an intuitive way to navigate through different options and perform actions without having to remember complex command-line arguments.

The interactive console is powered by the `console_menu.js` module, which leverages Node.js's `readline` library to facilitate user input and menu navigation. When the Autopilot CLI is launched, the interactive console becomes the primary interface through which users interact with the tool. This interface is designed to guide users through various tasks, such as setting up projects, generating documentation, and creating unit tests, all within a cohesive and streamlined environment.

Upon starting the CLI, users are greeted with a welcome message and a prompt to either set up a new project or load an existing configuration. During the setup phase, the console collects essential information such as the project path and entry point, ensuring that the CLI has all the necessary details to function correctly. This initial setup is crucial for the subsequent operations, as it defines the context in which the CLI will operate.

Once the setup is complete or an existing configuration is loaded, the main menu is presented. This menu is the central hub from which users can access various functionalities of the Autopilot CLI. The main menu offers several options, including:

1. **Select File**: Allows users to choose a specific file from the project directory. This option is particularly useful for tasks that require file-level granularity, such as generating JSDoc comments or running unit tests on individual files.

2. **Generate JSDoc**: Provides options for generating missing JSDoc comments for functions and variables within the selected file. This feature leverages the `generate_missing_js_doc.js` and `generate_target_js_doc.js` modules to ensure comprehensive documentation coverage.

3. **Generate Unit Tests**: Facilitates the creation of unit tests for the selected file or target. Users can choose to generate a single unit test or an entire suite, depending on their needs. This functionality is powered by the `generate_single_unit_test.js` and `generate_unit_test_suite.js` modules.

4. **Check File Status**: Displays the status of variables and functions within the selected file, indicating whether they have associated JSDoc comments and unit tests. This feature helps users quickly identify areas that require attention, ensuring that their codebase is well-documented and thoroughly tested.

5. **Edit Configuration**: Opens the configuration file in the user's preferred text editor, allowing them to make changes to the CLI settings. This option is useful for adjusting project paths, entry points, or other configuration parameters without having to restart the CLI.

The interactive console is designed to be highly responsive and user-friendly. It supports various input methods, including single-line and multi-line inputs, ensuring that users can provide the necessary information in the most convenient manner. Additionally, the console includes safeguards to prevent accidental exits or data loss. For example, when performing critical actions, the console may prompt users to confirm their choices, reducing the risk of unintended operations.

One of the standout features of the interactive console is its ability to handle user input asynchronously. This design allows the CLI to remain responsive even when performing time-consuming tasks, such as loading large projects or generating extensive documentation. By leveraging asynchronous functions, the console can provide real-time feedback and progress updates, enhancing the overall user experience.

Furthermore, the interactive console is extensible, allowing developers to add new menu options and functionalities as needed. This extensibility is achieved through a modular design, where each menu option is encapsulated within its own module. This approach ensures that the console remains maintainable and can easily adapt to evolving project requirements.

In summary, the interactive console is a cornerstone of the Autopilot CLI, offering a seamless and intuitive interface for managing JavaScript projects. Its comprehensive menu options, responsive design, and extensibility make it an invaluable tool for developers seeking to streamline their workflow and ensure the quality of their codebase. Whether setting up a new project, generating documentation, or creating unit tests, the interactive console provides all the necessary tools to get the job done efficiently and effectively.
