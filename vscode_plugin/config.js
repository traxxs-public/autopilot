
const self = {

    config: {
        test_framework: 'jest',
        unit_test_output_path: 'test/unit',
        chat_options: {
            generate_code: {
                provider: 'groq',
                model: 'llama3-70b-8192'
            },
            modify_code: {
                provider: 'groq',
                model: 'llama3-70b-8192'
            },
            js_doc: {
                provider: 'groq',
                model: 'llama3-70b-8192'
            },
            write_unit_test: {
                // provider: 'groq',
                // model: 'llama3-8b-8192'
                provider: 'openai',
                model: 'gpt-4o'
            },
            generate_unit_test_cases: {
                provider: 'groq',
                model: 'llama3-70b-8192'
            }
        }
    },

}

module.exports = self;