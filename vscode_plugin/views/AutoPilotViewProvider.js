const vscode = require('vscode');

class AutoPilotViewProvider {

    static viewType = 'autopilotView';

    constructor(extensionUri) {
        console.log('AutoPilotViewProvider.constructor');
        this._extensionUri = extensionUri;
    }

    resolveWebviewView(webviewView, context, _token) {
        try{
            console.log('AutoPilotViewProvider.resolveWebviewView');
            webviewView.webview.options = {
                enableScripts: true,
                localResourceRoots: [this._extensionUri]
            };
    
            webviewView.webview.html = this.getHtmlContent();
        }catch(err){
            console.log(err);
        }
        
    }

    getHtmlContent() {
        return `
            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>AutoPilot</title>
            </head>
            <body>
                <h1>AutoPilot</h1>
                <p>Use this tool to generate code based on your selection.</p>
                <button onclick="generateCode()">Generate Code</button>

                <script>
                    const vscode = acquireVsCodeApi();
                    function generateCode() {
                        vscode.postMessage({
                            command: 'generateCode'
                        });
                    }
                </script>
            </body>
            </html>
        `;
    }
}

module.exports = AutoPilotViewProvider;