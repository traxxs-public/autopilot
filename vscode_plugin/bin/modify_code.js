const plugin_utils = require('../utils'),
    plugin_config = require('../config'),
    { jobs } = require('../../autopilot'),
    token_utils = require('../../lib/ai/token_utils'),
    Project = require('../../parsing/project'),
    vscode = require('vscode');

const self = async () => {

    try{
        token_utils.resetSessionTokenUsage();
        let ctx = await plugin_utils.getRightClickContext();

        let project = new Project("my_project", ctx.projectPath),
            file_path = ctx.filePath.replace(ctx.projectPath + '/', ''),
            file = project.loadFile(file_path, {parse_loaded_files: true}),
            target = file.getTargetFromLineIndex(ctx.lineNumber - 1);

        if (!target) {
            vscode.window.showErrorMessage('No target found at line ' + ctx.lineNumber);
            return;
        }

        let extract = file.getTargetExtract(target.type, target.name);

        if(!extract){
            vscode.window.showErrorMessage('Error extracting code for ' + target.name);
            return;
        }

        let specifications = await vscode.window.showInputBox({
            prompt: 'Specifications for the code to generate',
        });

        let code_context = '// ' + file.name + '\n' + file.content;

        vscode.window.showInformationMessage('Generating code for ' + file_path);

        let context = {
            code: code_context,
            modify_code: extract.content,
            specifications: specifications,
            chat_options: plugin_config.config.chat_options.modify_code
        };

        try{
            let result = await jobs.modify_code(context);
            file.replaceLines(extract.start, extract.end, result.code);
            let cost = token_utils.computeSessionTotalCost();
            vscode.window.showInformationMessage('Code modified for ' + target.name + ' (Cost: ' + cost + '$)');
        }catch(err){
            console.log('Error generating code...');
            console.log(err);
            vscode.window.showErrorMessage('Error generating code...');
            return;
        }
    }catch(e){
        console.log(e);
    }

}

module.exports = self;