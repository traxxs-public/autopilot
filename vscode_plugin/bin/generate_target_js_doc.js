const plugin_utils = require('../utils'),
    plugin_config = require('../config'),
    { jobs } = require('../../autopilot'),
    token_utils = require('../../lib/ai/token_utils'),
    Project = require('../../parsing/project'),
    vscode = require('vscode');

const self = async () => {

    try {
        token_utils.resetSessionTokenUsage();

        let ctx = await plugin_utils.getRightClickContext();

        let project = new Project("my_project", ctx.projectPath),
            file_path = ctx.filePath.replace(ctx.projectPath + '/', ''),
            file = project.loadFile(file_path, { parse_loaded_files: true }),
            target = file.getTargetFromLineIndex(ctx.lineNumber - 1);

        if (!target) {
            vscode.window.showErrorMessage('No target found at line ' + ctx.lineNumber);
            return;
        }

        vscode.window.showInformationMessage('Generating JSDoc for ' + file_path + " => " + target.name);

        let context = {
            project: project,
            file: file,
            chat_options: plugin_config.config.chat_options.js_doc
        };

        try {
            let js_doc = await generateJSDoc(context, target);
            let file_target = target.type === 'function' ? file.function_map[target.name] : file.variable_map[target.name];
            if (file_target.js_doc) {
                file.replace('\n' + file_target.js_doc, "", false);
            }
            file.insertBeforeLine([target.name + ":", target.name + " :"], js_doc);
            let cost = token_utils.computeSessionTotalCost();
            vscode.window.showInformationMessage('JSDoc generated for ' + target.name + ' (Cost: ' + cost + '$)');
        } catch (err) {
            console.log('Error generating JSDoc...');
            vscode.window.showErrorMessage('Error generating JSDoc...');
            return;
        }
    } catch (e) {
        console.log(e);
    }

}

async function generateJSDoc(context, target, try_count = 0) {
    try {
        let code;
        if (target.type === 'function') {
            context.function_name = target.name;
            code = (await jobs.extract_module_function.set({
                include_dependencies: true,
                max_depth: 1,
                exclude_target_function_js_doc: true
            })(context)).code;
        } else if (target.type === 'variable') {
            context.variable_name = target.name;
            code = (await jobs.extract_module_variable.set({
                include_dependencies: true,
                max_depth: 1,
                exclude_target_variable_js_doc: true
            })(context)).code;
        }

        context.code = code;

        let js_doc;
        if (target.type === 'function') {
            js_doc = (await jobs.generate_function_js_doc(context)).js_doc;
        } else if (target.type === 'variable') {
            js_doc = (await jobs.generate_variable_js_doc(context)).js_doc;
        }
        return js_doc;
    } catch (err) {
        if (try_count < 2) {
            return generateJSDoc(context, target, try_count + 1);
        } else {
            throw err;
        }
    }
}

module.exports = self;