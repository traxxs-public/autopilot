const plugin_utils = require('../utils'),
    plugin_config = require('../config'),
    { jobs } = require('../../autopilot'),
    token_utils = require('../../lib/ai/token_utils'),
    Project = require('../../parsing/project'),
    utils = require('../../lib/utils'),
    vscode = require('vscode');

const self = async () => {

    try{
        token_utils.resetSessionTokenUsage();
        let ctx = await plugin_utils.getRightClickContext();

        let project = new Project("my_project", ctx.projectPath),
            file_path = ctx.filePath.replace(ctx.projectPath + '/', ''),
            file = project.loadFile(file_path, {parse_loaded_files: true}),
            target_line_index = ctx.lineNumber - 1;

        let specifications = await vscode.window.showInputBox({
            prompt: 'Specifications for the code to complete',
        });

        let code_lines = file.content.split('\n');

        code_lines.splice(target_line_index, 0, "// [CODE INSERTION POINT]");

        let code_context = file.project_path + ":\n```javascript\n" + code_lines.join('\n') + "\n```";

        vscode.window.showInformationMessage('Generating code for ' + file_path);

        let context = {
            project: project,
            file: file,
            code: code_context,
            specifications: specifications,
            chat_options: plugin_config.config.chat_options.generate_code
        };

        try{
            let result = await jobs.implement_code(context);
            let code = utils.removeMatchingLines(result.code, "// [CODE INSERTION POINT]");
            file.insertBeforeLineIndex(target_line_index, code);
            let cost = token_utils.computeSessionTotalCost();
            vscode.window.showInformationMessage('Code generated for ' + file_path + ' (Cost: ' + cost + '$)');
        }catch(err){
            console.log('Error generating code...');
            console.log(err);
            vscode.window.showErrorMessage('Error generating code...');
            return;
        }
    }catch(e){
        console.log(e);
    }

}

module.exports = self;