const fse = require('fs-extra'),
    vscode = require('vscode'),
    Project = require('../../parsing/project'),
    plugin_utils = require('../utils'),
    plugin_config = require('../config'),
    { jobs, pipe } = require('../../autopilot'),
    token_utils = require('../../lib/ai/token_utils');

const self = async () => {

    try {
        token_utils.resetSessionTokenUsage();
        let ctx = await plugin_utils.getRightClickContext();

        let project = new Project("my_project", ctx.projectPath),
            file_path = ctx.filePath.replace(ctx.projectPath + '/', ''),
            file = project.loadFile(file_path, { parse_loaded_files: true }),
            target = file.getTargetFromLineIndex(ctx.lineNumber - 1);

        let err = project.matchTestFrameworkRequirements(plugin_config.config.test_framework, false);
        if(err){
            vscode.window.showErrorMessage(err);
            return;
        }

        if (!target) {
            vscode.window.showErrorMessage('No target found at line ' + ctx.lineNumber);
            return;
        } else if (target.type !== 'function') {
            vscode.window.showErrorMessage('Target is not a function');
            return;
        }

        vscode.window.showInformationMessage('Generating Unit tests for ' + file_path + " => " + target.name);

        let unit_test_cases = await getTestCases(project, file, target);
        if (!unit_test_cases) {
            vscode.window.showErrorMessage('Error generating Unit Test cases...');
            return;
        }else{
            vscode.window.showInformationMessage('Generated Unit Test cases: ' + unit_test_cases.length);
        }

        let generated_count = 0;

        let context = {
            project: project,
            file: file,
            function_name: target.name,
            unit_test_cases,
            chat_options: plugin_config.config.chat_options.write_unit_test,
            skip_failed_tests: true,
            on_test_implemented: () => {
                generated_count++;
                vscode.window.showInformationMessage('Generated ' + generated_count + ' / ' + unit_test_cases.length + ' Unit Tests');
            }
        };

        let result;

        try {
            result = await jobs.implement_unit_test_suite(context);
        } catch (err) {
            vscode.window.showErrorMessage('Error generating Unit Tests...');
            return;
        }

        let test_code = result.test_code,
            failed_count = 0;
        
        for (let i = 0; i < unit_test_cases.length; i++) {
            let passing = result.passing_test_list[i];
            if (!passing) {
                failed_count++;
            }
        }

        await applyUnitTests(project, file, target.name, test_code);

        let cost = token_utils.computeSessionTotalCost();
        vscode.window.showInformationMessage('All Unit Tests generated (' + (unit_test_cases.length - failed_count) + ' / ' + unit_test_cases.length + ' passing) (Cost: ' + cost + '$)');

    } catch (e) {
        console.log(e);
    }

}

async function getTestCases(project, file, target) {
    console.log('Generating Unit Test cases...');
    let context = {
        project: project,
        file: file,
        function_name: target.name,
        chat_options: plugin_config.config.chat_options.generate_unit_test_cases
    };
    let result = await pipe([
        jobs.extract_module_function.set({
            include_dependencies: true,
            max_depth: 0,
            only_prototypes: true
        }),
        jobs.generate_unit_test_cases
    ])(context);
    let test_cases = result.unit_test_cases;
    return test_cases;
}

async function applyUnitTests(project, file, function_name, test_code) {
    let unit_test_output_path = plugin_utils.getUnitTestsOutputPath(project, file, function_name, false);
    path = project.root_path + '/' + unit_test_output_path;
    fse.outputFileSync(path, test_code);
}

module.exports = self;